#ifndef __DAMDACORESDK_H__
#define __DAMDACORESDK_H__

#define 	_ARDUINO_
#define 	_DEBUG_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Arduino.h>
#include <jansson.h>
#include <MQTT.h>
#include <WiFiClientSecure.h>

#define DAMDA_SDK_VERSION "damda_Arduino_sdk_v1.0"

#define BUF_LEN 1024
#define QOS 1
#define MESSAGE_TYPE_RESPONSE 1
#define MESSAGE_TYPE_READ 2
#define MESSAGE_TYPE_WRITE 3
#define MESSAGE_TYPE_EXECUTE 4
#define MESSAGE_TYPE_DELETE_SYNC 5
#define MESSAGE_TYPE_DELETE_ASYNC 6
#define MESSAGE_TYPE_FIRMWARE_DOWNLOAD 7
#define MESSAGE_TYPE_READ_ASYNC 8
#define MESSAGE_TYPE_WRITE_ASYNC 9
#define MESSAGE_TYPE_EXECUTE_ASYNC 10

#define TOPIC_VERSION "1.0"
#define PROFILE_VERSION "1.0"
#define SERVICE_PROTOCOL_VERSION "1.0"

#define MAX_RESOURCE_SIZE 100

#define SID_BUFFER_LEN 100
#define ERROR_CODE_BUFFER_LEN 10
#define RETURN_CODE_BUFFER_LEN 10
#define TIME_BUFFER_LEN 15

typedef struct connectionInfo{
	const char* serverAddr;
	const char* serverPort;
	const char* apSsid;
	const char* apPassword;
	const char* authKey;
}DAMDA_connectionInfo;

typedef struct deviceStatusInfo{
	char* manufacturer;
	char* model;
	char* serial;
	char* firmwareVersion;
} DeviceStatusInfo;

typedef struct responseMessageInfo{

	#ifdef _ARDUINO_
		//char Operation[SID_BUFFER_LEN];
	
		// String sid;
		// String did;
		// String resourceUri[MAX_RESOURCE_SIZE];
		// String stringValue[MAX_RESOURCE_SIZE];
		// String time_data[MAX_RESOURCE_SIZE];
		// String firmwareDownloadUrl;
		// String firmwareVersion;
	    char sid[SID_BUFFER_LEN];
	    int seq;
	    char* resourceUri[MAX_RESOURCE_SIZE];
	    char* stringValue[MAX_RESOURCE_SIZE];
	    char* time[MAX_RESOURCE_SIZE];
	    char* firmwareDownloadUrl;
	    char* firmwareVersion;
	#elif _LINUX_
		char sid[SID_BUFFER_LEN];
		char* resourceUri[MAX_RESOURCE_SIZE];
		char* stringValue[MAX_RESOURCE_SIZE];
		char* time[MAX_RESOURCE_SIZE];
		char* firmwareDownloadUrl;
		char* firmwareVersion;
	#endif
	int returnCode;
	int errorCode;

	int responseType;
	int responseArraySize;

} ResponseMessageInfo;

typedef struct notifyParamter{
	char* resourceUri;
	char* stringValue;
	char* time;
} NotifyParameter;

typedef void (*MSG_ARRIVED_CALLBACK_FUNCTION)(ResponseMessageInfo*);
//typedef void (*CONNECT_FAIL_CALLBACK_FUNCTION)(char* errMessage);
//extern MQTTClient client;
extern NotifyParameter DeviceNotifyParameter[10];
extern char buffer[BUF_LEN];
//struct sockaddr_in server_addr, client_addr;

extern int server_fd, client_fd;

extern int len, msg_size;

extern int connectionFailCallbackStatus;


extern int receiveServerInfoStatus;

extern const char* serverAddr;
extern const char* serverPort;
extern const char* apSsid;
extern const char* apPassword;
extern const char* authKey;

//pthread_t httpThread;
//int httpThread_id;
//int httpThread_status;

//int firmwareStep;
//int runningFirmwareUpdate;

#ifdef __cplusplus
extern "C" {
#endif

//char* topicVersion = "1.0";
//char* profileVersion = "1.0";
//char* serviceProtocolVersion = "1.0";

/*
extern DAMDA_connectionInfo damda_connInfo;
extern DeviceStatusInfo deviceStatus;
*/
char* damda_getSDKVersion();

DAMDA_connectionInfo getConnectionInfo();

DeviceStatusInfo getDeviceInfo();

void damda_setupDeviceInfo(const char* manufacturer, const char* model, const char* serial, const char* firmwareVersion, const char* deviceType);

//char* damda_getDeviceId();

//void damda_setDeviceType(char* myDeviceType);

int damda_isReceiveServerInfo();

//void damda_setMQTTConnectionFailCallback(CONNECT_FAIL_CALLBACK_FUNCTION func);

void damda_setMQTTMsgReceiveCallback(MSG_ARRIVED_CALLBACK_FUNCTION func);

//int damda_ConnectMQTT(const char* hostUrl,const char* portNumber); -> Core

void damda_MQTT_Connect(char* mqttUser,char* mqttPassword,char* mqttAddress,int mqttPort,char* device_type);

void damda_DisConnectMQTT();

void damda_destroyMQTT();

int damda_IsMQTTConnected();

int damda_Req_RegisterDevice();

int damda_Req_DeregisterDevice();

int damda_Req_DeleteDevice();

int damda_Req_UpdateDeviceInfo();

int damda_Req_SeverTimeInfo();

int damda_Notify_DeviceStatus(const char* notificationType,NotifyParameter notifyParams[],int arrayCount);

int damda_Rsp_RemoteControl(int controlType,const char* sid,const char* returnCode,char* resourceUri[],char* stringValue[],int arrayCount);

int damda_Rsp_RemoteControl_noEntity(int controlType,const char* sid,const char* returnCode);

int damda_Rsp_RemoteDelDevice(const char* sid, const char* returnCode);

int damda_Req_FirmwareDownload(char* downloadPath,char* fileName,char* firmwareDownloadUrl);

void damda_SetConnectInfo(char* server_addr,char* server_port,char* ap_ssid, char* ap_password,char* auth_key);

void damda_messageReceived_callback_loop();

/*
#ifdef _ARDUINO_
String getSid(String message);
String getDid(String message);
String getOperation(String message);
String getReturnCode(String message);
String getErrorCode(String message);
String getResourceUri(String message,size_t index);
String getStringValue(String message,size_t index);
int getNArraySize(String message);
String getTime(String message,size_t index);
String getServiceId(String topic);
String getMsgType(String topic);
String getOriginId(String topic);
String getTargetId(String topic);
String getTopicOprId(String topic);


#elif _LINUX_*/
//char* 	getSid(char* message);
//int 	getSeq(char* message);
char* 	getDid(char* message);
//char* 	getOperation(char* message,char* operation);
//int 	getReturnCode(char* message);
//int 	getErrorCode(char* message);
//char* 	getResourceUri(char* message,size_t index);
//char* 	getStringValue(char* message,size_t index);
int 	getNArraySize(char* message);
//char* 	getTime(char* message,size_t index);
char* 	getServiceId(char* topic);
//char* 	getMsgType(char* topic);
//char* 	getOriginId(char* topic);
//char* 	getTargetId(char* topic);
//char* 	getTopicOprId(char* topic);
int 	getMessageType(char* topic,char* message);
//#endif
int getSid(char* message,char* mSid);
int getSeq(char* message,int* mSeq);
int getOperation(char* message,char* operation);
int getReturnCode(char* message,int* mReturnCode);
int getErrorCode(char* message,int* mErrorCode);
int getResourceUri(char* message,size_t index,char* mResourceUri);
int getNArraySize(char* message);
int getStringValue(char* message,size_t index,char* mStringValue);
int getTime(char* message,size_t index,char* mTime);
int getStringValueByResourceUri(char* message,char* n,char* mStringValue);
void getMsgType(char* topic,char* mMsgType);
void getOriginId(char* topic,char* mOriginId);
void getTargetId(char* topic,char* mTargetId);
void getTopicOprId(char* topic,char* mTopicOprId);



int isFirmwareUpdateMessage(char* message);
char *httpParser(char *s, const char *olds, const char *news);
void httpResponseController(char* header,char* body);
void damda_httpServerInit(int port);
void damda_httpServerThreadStart();

#ifdef __cplusplus
}
#endif

#endif
