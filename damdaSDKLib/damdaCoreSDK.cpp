#include "damdaCoreSDK.h"

	char deviceId[50];
	char subscribeTopic[100];
	char damda_deviceType[20];
	//DeviceStatusInfo DeviceStatus;
	/*#ifdef _ARDUINO_
	void __printf(const char *s,...){
	    va_list args;
	    va_start(args, s);
		int n = vsn_printf(NULL, 0, s, args);
		char *str = new char[n+1];
		vsprintf(str, s, args);
	    va_end(args);
	    Serial.print(str);
	    delete [] str;
	}
	#endif
	*/
	int sequence = 0 ;
	const int MAX_SEQUENCE = 60000;
	MSG_ARRIVED_CALLBACK_FUNCTION callbackFunc = NULL;
	
	WiFiClientSecure net;
		
	MQTTClient client(2048);
	int messageArrivedCallbackStatus;
	DeviceStatusInfo deviceStatus;
	DAMDA_connectionInfo damda_connInfo;
int damda_Req_RegisterDevice(){
	//timer1_disable();
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3,*eJsonObject4,*eJsonObject5,*eJsonObject6,*eJsonObject7;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3,*nJson4,*nJson5,*nJson6,*nJson7;
	json_t *svJson1,*svJson2,*svJson3,*svJson4,*svJson5,*svJson6,*svJson7;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2","/3/0/3","/3/0/997","/3/0/998","/3/0/999"};

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();

    sprintf(topic,"%s/sync/%s/iot-server/register/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("rg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

    eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	svJson1 = json_string(deviceStatus.manufacturer);
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);

    eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	svJson2 = json_string(deviceStatus.model);
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);

    eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	svJson3 = json_string(deviceStatus.serial);
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);

    eJsonObject4 = json_object();
	nJson4 = json_string(n[3]);
	svJson4 = json_string(deviceStatus.firmwareVersion);
    json_object_set(eJsonObject4,"n",nJson4);
    json_object_set(eJsonObject4,"sv",svJson4);
    json_array_append_new(eArray,eJsonObject4);

    eJsonObject5 = json_object();
	nJson5 = json_string(n[4]);
	svJson5 = json_string(TOPIC_VERSION);
    json_object_set(eJsonObject5,"n",nJson5);
    json_object_set(eJsonObject5,"sv",svJson5);
    json_array_append_new(eArray,eJsonObject5);

    eJsonObject6 = json_object();
	nJson6 = json_string(n[5]);
	svJson6= json_string(PROFILE_VERSION);
    json_object_set(eJsonObject6,"n",nJson6);
    json_object_set(eJsonObject6,"sv",svJson6);
    json_array_append_new(eArray,eJsonObject6);

    eJsonObject7 = json_object();
	nJson7 = json_string(n[6]);
	svJson7= json_string(SERVICE_PROTOCOL_VERSION);
    json_object_set(eJsonObject7,"n",nJson7);
    json_object_set(eJsonObject7,"sv",svJson7);
    json_array_append_new(eArray,eJsonObject7);

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
   /* MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/
	bool result_ok = client.publish(topic, result);

	#ifdef _DEBUG_
		Serial.print("[damda] Info : topic - ");
		Serial.println(topic);
		Serial.print("[damda] Info : sendMessage - ");
		Serial.println(result);
	#endif

	free(result);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(nJson4);
	json_decref(nJson5);
	json_decref(nJson6);
	json_decref(nJson7);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(svJson4);
	json_decref(svJson5);
	json_decref(svJson6);
	json_decref(svJson7);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(eJsonObject4);
	json_decref(eJsonObject5);
	json_decref(eJsonObject6);
	json_decref(eJsonObject7);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
	Serial.print("[damda] Send OK ");
    return resultSequence;
    //timer1_enable(1,0,1);

}

int damda_Req_DeregisterDevice(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;

    #ifdef _ARDUINO_
    #elif _LINUX_
    	char* copyDeviceId = malloc(sizeof(deviceId)+1);
    #endif

    char* ptr;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3;
	json_t *svJson1,*svJson2,*svJson3;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2"};

    #ifdef _ARDUINO_
	char* sv[7] = {deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial};
    #endif

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
	root = json_object();
	msg = json_object();
	eArray = json_array();

	#ifdef _ARDUINO_
    #elif _LINUX_
    	strcpy(copyDeviceId,deviceId);
    	ptr = strtok(copyDeviceId,"-");
    #endif

    sprintf(topic,"%s/sync/%s/iot-server/unregister/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("urg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

	eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	#ifdef _ARDUINO_
	svJson1 = json_string(sv[0]);
    #elif _LINUX_
	svJson1 = json_string(ptr);
	#endif
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif


	eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	#ifdef _ARDUINO_
	svJson2 = json_string(sv[1]);
    #elif _LINUX_
	svJson2 = json_string(ptr);
	#endif
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	#ifdef _ARDUINO_
	svJson3 = json_string(sv[2]);
    #elif _LINUX_
	svJson3 = json_string(ptr);
	#endif
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/
	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		Serial.print("[damda] Info : topic - ");
		Serial.println(topic);
		Serial.print("[damda] Info : sendMessage - ");
		Serial.println(result);
	#endif

	#ifdef _ARDUINO_
    #elif _LINUX_
    free(copyDeviceId);
    #endif

    free(result);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);

	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
  	return resultSequence;
}

int damda_Req_DeleteDevice(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;

    #ifdef _ARDUINO_
    #elif _LINUX_
    	char* copyDeviceId = malloc(sizeof(deviceId)+1);
    #endif

    char* ptr;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3;
	json_t *svJson1,*svJson2,*svJson3;

    char sequenceString[10];

    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2"};

    #ifdef _ARDUINO_
	char* sv[7] = {deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial};
    #endif

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

	root = json_object();
	msg = json_object();
	eArray = json_array();

	#ifdef _ARDUINO_
    #elif _LINUX_
    	strcpy(copyDeviceId,deviceId);
    	ptr = strtok(copyDeviceId,"-");
    #endif

    sprintf(topic,"%s/sync/%s/iot-server/deregister/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);

	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("drg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

	eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	#ifdef _ARDUINO_
	svJson1 = json_string(sv[0]);
    #elif _LINUX_
	svJson1 = json_string(ptr);
	#endif
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	#ifdef _ARDUINO_
	svJson2 = json_string(sv[1]);
    #elif _LINUX_
	svJson2 = json_string(ptr);
	#endif
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	#ifdef _ARDUINO_
	svJson3 = json_string(sv[2]);
    #elif _LINUX_
	svJson3 = json_string(ptr);
	#endif
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

	#ifdef _ARDUINO_
    #elif _LINUX_
    	free(copyDeviceId);
    #endif

    free(result);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}

int damda_Req_UpdateDeviceInfo(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3,*eJsonObject4,*eJsonObject5,*eJsonObject6,*eJsonObject7;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3,*nJson4,*nJson5,*nJson6,*nJson7;
	json_t *svJson1,*svJson2,*svJson3,*svJson4,*svJson5,*svJson6,*svJson7;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2","/3/0/3","/3/0/997","/3/0/998","/3/0/999"};

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

    root = json_object();
    msg = json_object();
    eArray = json_array();
    sequence = sequence % MAX_SEQUENCE + 1;
    if(sequence != NULL){
    	sprintf(sequenceString,"%d",sequence);
	}
	if(deviceId != NULL){
	    sprintf(topic,"%s/sync/%s/iot-server/update/json",damda_deviceType,deviceId);
	}
	sidJson = json_string("3");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("up");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

    eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	svJson1 = json_string(deviceStatus.manufacturer);
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);


    eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	svJson2 = json_string(deviceStatus.model);
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);


    eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	svJson3 = json_string(deviceStatus.serial);
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);


    eJsonObject4 = json_object();
	nJson4 = json_string(n[3]);
	svJson4 = json_string(deviceStatus.firmwareVersion);
    json_object_set(eJsonObject4,"n",nJson4);
    json_object_set(eJsonObject4,"sv",svJson4);
    json_array_append_new(eArray,eJsonObject4);


    eJsonObject5 = json_object();
	nJson5 = json_string(n[4]);
	svJson5 = json_string(TOPIC_VERSION);
    json_object_set(eJsonObject5,"n",nJson5);
    json_object_set(eJsonObject5,"sv",svJson5);
    json_array_append_new(eArray,eJsonObject5);


    eJsonObject6 = json_object();
	nJson6 = json_string(n[5]);
	svJson6= json_string(PROFILE_VERSION);
    json_object_set(eJsonObject6,"n",nJson6);
    json_object_set(eJsonObject6,"sv",svJson6);
    json_array_append_new(eArray,eJsonObject6);


    eJsonObject7 = json_object();
	nJson7 = json_string(n[6]);
	svJson7= json_string(SERVICE_PROTOCOL_VERSION);
    json_object_set(eJsonObject7,"n",nJson7);
    json_object_set(eJsonObject7,"sv",svJson7);
    json_array_append_new(eArray,eJsonObject7);

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

   	free(result);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(nJson4);
	json_decref(nJson5);
	json_decref(nJson6);
	json_decref(nJson7);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(svJson4);
	json_decref(svJson5);
	json_decref(svJson6);
	json_decref(svJson7);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(eJsonObject4);
	json_decref(eJsonObject5);
	json_decref(eJsonObject6);
	json_decref(eJsonObject7);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}

int damda_Req_SeverTimeInfo(){
    int rc;
    int ch;
    char topic[100] = "";
    char* result;
    json_t *root, *msg;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
    char sequenceString[10];

    int resultSequence;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif

		#endif
		return -7;
	}

    root = json_object();
    msg = json_object();

    sprintf(topic,"%s/sync/%s/iot-server/timesync/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("tisyn");

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);

    resultSequence = sequence;
    return resultSequence;
}

int damda_Notify_DeviceStatus(const char* notificationType,NotifyParameter notifyParams[],int arrayCount){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
	const int cArrayCount = arrayCount;
    json_t *root, *msg, *eArray;
	json_t* eJsonObject[cArrayCount];
	json_t* nJson[cArrayCount];
	json_t* svJson[cArrayCount];
	json_t* tiJson[cArrayCount];
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
    char sequenceString[10];
    int resultSequence;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();
	sprintf(topic,"%s/sync/%s/iot-server/notify/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string(notificationType);
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("n");

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);
    for(i=0;i<arrayCount;i++){
	    eJsonObject[i] = json_object();
		nJson[i] = json_string(notifyParams[i].resourceUri);
		svJson[i] = json_string(notifyParams[i].stringValue);
		tiJson[i] = json_string(notifyParams[i].time);
	    json_object_set(eJsonObject[i],"n",nJson[i]);
	    json_object_set(eJsonObject[i],"sv",svJson[i]);
	    json_object_set(eJsonObject[i],"ti",tiJson[i]);
	    json_array_append_new(eArray,eJsonObject[i]);
	}

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	for(i=0;i<arrayCount;i++){
		json_decref(eJsonObject[i]);
		json_decref(nJson[i]);
		json_decref(svJson[i]);
		json_decref(tiJson[i]);
	}

	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}

int damda_Rsp_RemoteControl(int controlType,const char* sid,const char* returnCode,char* resourceUri[],char* stringValue[],int arrayCount){
    int ch;
    char topic[100];
    int i=0;
    char* result;
	const int cArrayCount = arrayCount;
    json_t *root, *msg, *eArray;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;
	json_t *eJsonObject[cArrayCount];
	json_t *nJson[cArrayCount];
	json_t *svJson[cArrayCount];

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();


    if(controlType == 2){
	    sprintf(topic,"%s/sync/%s/iot-server/read/json",damda_deviceType,deviceId);
    }
    else if(controlType == 3){
	    sprintf(topic,"%s/sync/%s/iot-server/write/json",damda_deviceType,deviceId);
    }
    else if(controlType == 4){
		sprintf(topic,"%s/sync/%s/iot-server/execute/json",damda_deviceType,deviceId);
    }
    else {
		printf("[damda] Error : invalid controlType \n");
	return -1;
    }
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	returnCodeJson = json_string("res");
	operationJson = json_string(returnCode);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",returnCodeJson);
    json_object_set(msg,"rc",operationJson);

	for(i=0;i<arrayCount;i++){
    eJsonObject[i] = json_object();
	nJson[i] = json_string(resourceUri[i]);
	svJson[i] = json_string(stringValue[i]);
    json_object_set(eJsonObject[i],"n",nJson[i]);
    json_object_set(eJsonObject[i],"sv",svJson[i]);
    json_array_append_new(eArray,eJsonObject[i]);
     }
    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	for(i=0;i<arrayCount;i++){
		json_decref(eJsonObject[i]);
		json_decref(nJson[i]);
		json_decref(svJson[i]);
	}
	json_decref(sidJson);
	json_decref(didJson);

	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    return 1;

}

int damda_Rsp_RemoteControl_noEntity(int controlType,const char* sid,const char* returnCode){
    int ch;
    char topic[100];
    int i=0;
    char* result;
    json_t *root, *msg, *eArray;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

    if(controlType == 2){
	    sprintf(topic,"%s/sync/%s/iot-server/read/json",damda_deviceType,deviceId);
    }
    else if(controlType == 3){
	    sprintf(topic,"%s/sync/%s/iot-server/write/json",damda_deviceType,deviceId);
    }
    else if(controlType == 4){
       	sprintf(topic,"%s/sync/%s/iot-server/execute/json",damda_deviceType,deviceId);
    }
    else if(controlType == 5){
       	sprintf(topic,"%s/sync/%s/iot-server/delete/json",damda_deviceType,deviceId);
    }
    else {
    	#ifdef _ARDUINO_
			Serial.println("invalid controlType\n");
		#elif _LINUX_
			printf("invalid controlType\n");
		#endif
	return -1;
    }

	#ifdef _ARDUINO_
		Serial.print("in Sid :");
		Serial.println(sid);
	#elif _LINUX_
		printf("in Sid : %s\n",sid);
	#endif

    root = json_object();
    msg = json_object();
    eArray = json_array();
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	operationJson = json_string("res");
	returnCodeJson = json_string(returnCode);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(msg,"rc",returnCodeJson);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif
	#endif

   	free(result);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(returnCodeJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    return 1;
}

int damda_Rsp_RemoteDelDevice(const char* sid, const char* returnCode){
    int rcInt;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

    root = json_object();
    msg = json_object();
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	operationJson = json_string("res");
	returnCodeJson = json_string(returnCode);

    sprintf(topic,"%s/sync/%s/iot-server/delete/json",damda_deviceType,deviceId);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(msg,"rc",returnCodeJson);

    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif
	#endif

    free(result);
	json_decref(sidJson);
	json_decref(didJson);

	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);

    return 1;
}

/*
int connectFirmwareDownload(int sockfd, struct sockaddr *saddr, int addrsize, int sec)
{
    int newSockStat;
    int orgSockStat;
    int res, n;
    fd_set  rset, wset;
    struct timeval tval;

    int error = 0;
    int esize;

    if ( (newSockStat = fcntl(sockfd, F_GETFL, NULL)) < 0 )
    {
        perror("[damda] Error : F_GETFL error");
        return -1;
    }

    orgSockStat = newSockStat;
    newSockStat |= O_NONBLOCK;


    if(fcntl(sockfd, F_SETFL, newSockStat) < 0)
    {
        perror("[damda] Error : F_SETLF error");
        return -1;
    }


    if((res = connect(sockfd, saddr, addrsize)) < 0)
    {
        if (errno != EINPROGRESS)
            return -1;
    }



    if (res == 0)
    {
        Serial.println("[damda] Info : Connect Success\n");
        fcntl(sockfd, F_SETFL, orgSockStat);
        return 1;
    }

    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    wset = rset;

    tval.tv_sec        = sec;
    tval.tv_usec    = 0;

    if ( (n = select(sockfd+1, &rset, &wset, NULL, NULL)) == 0)
    {
        // timeout
        errno = ETIMEDOUT;
        return -1;
    }

    if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset) )
    {
        esize = sizeof(int);
        if ((n = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&esize)) < 0)
            return -1;
    }
    else
    {
        perror("[damda] Error : Socket Not Set");
        return -1;
    }


    fcntl(sockfd, F_SETFL, orgSockStat);
    if(error)
    {
        errno = error;
        perror("[damda] Error : Socket");
        return -1;
    }

    return 1;
}

int damda_Req_FirmwareDownload(char* downloadPath,char* fileName,char* firmwareDownloadUrl){

	if(strlen(firmwareDownloadUrl) <=0 ){
		Serial.println("firmwareDownloadUrl is empty\n");
		return -2;
	}

	char* n[2];
	char* sv[2];
	int socket_desc;
	char* message;
	char server_reply[1024];
	int total_len = 0;

	int headerflag = 0;
	FILE* file = NULL;
	struct sockaddr_in server;
	char* withoutHeader;
	int urlLen  = strlen(firmwareDownloadUrl)+1;
        char* url = (char*)malloc(urlLen+1);
	char* originUrl = url;
	strcpy(url,firmwareDownloadUrl);
        char* url2 = url;
        char* url3 = url;
        char* result;
        char host[50];
        int port;
        char* endpoint;
        int position;
        int substringlen;
        struct hostent *host_entry;
	DIR* dir = opendir(downloadPath);
	int downloadPathlen;
	int filenamePathlen;
	char* absoluteFilePath;
	char* headerToken;
        char* ptr;
        int contentLength;
	char server_reply_copy[1024];
	if(!dir){
		Serial.println("[damda] Error : directory doesn't exist\n");
		return -1;
	} else {
		closedir(dir);
	}
        url = strstr(url,"//")+2;
	if(strstr(url,":")!=NULL){
        	port = atoi(strstr(url,":")+1);
	} else {
		port = 80;
	}
        endpoint = strstr(url,"/");

        url2 = strstr(url2,"//")+2;

	if(strstr(url,":")!=NULL){
        	result = strstr(url2,":");
	} else {
		result = strstr(url2,"/");
	}
        position = result - url2;
        substringlen = strlen(url2) - position;

        url3 = strstr(url3,"//")+2;
        strncpy(host,url3,position);
        host[position] = '\0';

        host_entry = gethostbyname(host);

        if ( !host_entry)
        {
                 Serial.println( "[damda] Error : unknown Host/n");
		return -1;
        }














	//download
	socket_desc = socket(AF_INET,SOCK_STREAM,0);

	if(socket_desc == -1){
		Serial.println("[damda] Error : couldn't create socket\n");

		return -1;
	}
	Serial.println("[damda] Info : firmwareDownload URL is %s\n",firmwareDownloadUrl);
	server.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr*)host_entry->h_addr_list[0]));
	server.sin_family = AF_INET;
	server.sin_port = htons(port);

	if(connectFirmwareDownload(socket_desc,(struct sockaddr*)&server,sizeof(server),3)<0){
		Serial.println("conn error\n");

		return -1;
	}
	message = (char*)malloc(urlLen+30);

	Serial.println(message,"GET %s HTTP/1.1\r\nHost:%s:%d\r\n\r\n",endpoint,host,port);
	if(send(socket_desc,message,strlen(message),0)<0){
		Serial.println("[damda] Error : send Error \n");

		return -1;
	}

	downloadPathlen = strlen(downloadPath);
	filenamePathlen = strlen(fileName);
	absoluteFilePath = (char*)malloc(downloadPathlen + filenamePathlen + 1);
	Serial.println(absoluteFilePath,"%s/%s",downloadPath,fileName);

	remove(absoluteFilePath);
	file = fopen(absoluteFilePath,"ab");
	if(file == NULL){
		Serial.println("[damda] Error : file can't open\n");
		free(absoluteFilePath);
		return -1;
	}
	while(1){
		int received_len = recv(socket_desc,server_reply,sizeof(server_reply),0);
		if(received_len<0){
			Serial.println("[damda] Error : received Fail\n");
			return -1;
		}

		if(received_len==0){
			Serial.println("[damda] Info : received END\n");
			break;
		}
		if(headerflag==0){
			strcpy(server_reply_copy,server_reply);
			Serial.println("Server_reply %s\n",server_reply_copy);

			ptr = strtok(server_reply_copy,"\r\n");
			while(ptr!=NULL){
				Serial.println("ptr is %s\n",ptr);
                		if(strncmp(ptr,"Content-Length: ",16)==0){
					Serial.println("in if ptr %s",ptr);
                        		ptr = strstr(ptr," ") + 1;
					Serial.println("in if ptr %s",ptr);
	                        	contentLength = atoi(ptr);
					break;
        	        	}
                		ptr = strtok(NULL,"\r\n");
        		}
			Serial.println("LENGTH OK");
			headerflag=1;
			if(strlen(server_reply) != 1024){
				withoutHeader = strstr(server_reply,"\r\n\r\n");
				withoutHeader = withoutHeader + 4;
				fwrite(withoutHeader, 1024 - strlen(server_reply) + strlen(withoutHeader),1,file);
				total_len= 1024 - strlen(server_reply) + strlen(withoutHeader);
			} else {

				withoutHeader = strstr(server_reply,"\r\n\r\n");
				withoutHeader = withoutHeader + 4;
				fwrite(withoutHeader,strlen(withoutHeader),1,file);
				total_len = strlen(withoutHeader);
			}
			headerflag=1;
		} else {
			fwrite(server_reply,received_len,1,file);
			total_len+= received_len;

		}

		Serial.println("[damda] Info : downloaded file size is %d\n",total_len);
		if(contentLength == total_len){
			break;
		}
	}
	fclose(file);
	close(socket_desc);
	free(absoluteFilePath);
	free(originUrl);
	free(message);
	return 1;
}
*/
char* damda_getSDKVersion(){
	return DAMDA_SDK_VERSION;
}

int damda_isReceiveServerInfo(){
	return receiveServerInfoStatus;
}

DAMDA_connectionInfo getConnectionInfo(){
	return damda_connInfo;
}


DeviceStatusInfo getDeviceInfo(){
	return deviceStatus;
}
/*char* damda_getDeviceId(){
	return deviceId;
}*/

void damda_setupDeviceInfo(const char* manufacturer, const char* model, const char* serial, const char* firmwareVersion, const char* deviceType){
	deviceStatus.manufacturer = (char*)manufacturer;
	deviceStatus.model = (char*)model;
	deviceStatus.serial = (char*)serial;
	deviceStatus.firmwareVersion = (char*)firmwareVersion;
	sprintf(deviceId,"%s-%s-%s",deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial);
	strcpy(damda_deviceType,deviceType);
}

char *httpParser(char *s, const char *olds, const char *news) {
	char *result, *sr;
	size_t i, count = 0;
	size_t oldlen = strlen(olds); if (oldlen < 1) return s;
	size_t newlen = strlen(news);

	if (newlen != oldlen) {
		for (i = 0; s[i] != '\0';) {
      		if (memcmp(&s[i], olds, oldlen) == 0) count++, i += oldlen;
      		else i++;
    	}
  	}
  	else i = strlen(s);

  	result = (char *) malloc(i + 1 + count * (newlen - oldlen));
  	if (result == NULL) return NULL;

  	sr = result;
  	while (*s) {
    	if (memcmp(s, olds, oldlen) == 0) {
      		memcpy(sr, news, newlen);
      		sr += newlen;
      		s  += oldlen;
    	}
    	else *sr++ = *s++;
  	}
  	*sr = '\0';

  	return result;
}
/*
void httpResponseController(char* header,char* body){
    char* url;
    char* endurl;
    char* method;
    json_error_t bodyjsonError;
    char *sendMsg = (char*)malloc(sizeof(char)*500);
    json_t* bodyJson;
    url = strstr(header," ");
    endurl = strstr(url+1," ");
    method = httpParser(header,url,"");
    url = httpParser(url,endurl,"");
    url = httpParser(url," ","");
	if(strcmp(url,"/api/v1/wifi/info/get")==0){
		sprintf(sendMsg,"HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"manufacturer\" : \"%s\",\"model\" : \"%s\",\"serial\" : \"%s\",\"support_type\":\"wifi\"}\r\n\r\n",deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial);

    	write(client_fd,sendMsg,strlen(sendMsg));
   }
   else if (strcmp(url,"/api/v1/wifi/link/start")==0){
		strcpy(sendMsg,"HTTP/1.1 200 OK\r\n\r\n");
	    body = httpParser(body,"\r\n","");
	    printf("receive Data : %s",body);
	    printf("send Data : %s",sendMsg);
	    bodyJson= json_loads(body,0,&bodyjsonError);

	    serverAddr = json_string_value(json_object_get(bodyJson,"server_addr"));
		serverPort = json_string_value(json_object_get(bodyJson,"server_port"));
		apSsid = json_string_value(json_object_get(bodyJson,"ap_ssid"));
		apPassword = json_string_value(json_object_get(bodyJson,"ap_password"));
		authKey = json_string_value(json_object_get(bodyJson,"auth_key"));
		damda_connInfo.serverAddr=serverAddr;
		damda_connInfo.serverPort=serverPort;
		damda_connInfo.apSsid=apSsid;
		damda_connInfo.apPassword=apPassword;
		damda_connInfo.authKey=authKey;
		receiveServerInfoStatus = 1;
	    printf("[damda] Info : server_addr %s.\n", damda_connInfo.serverAddr);
		printf("[damda] Info : server_port %s.\n", damda_connInfo.serverPort);
		printf("[damda] Info : ap_ssid %s.\n", damda_connInfo.apSsid);
		printf("[damda] Info : ap_password %s.\n", damda_connInfo.apPassword);
		printf("[damda] Info : auth_key %s.\n", damda_connInfo.authKey);
		write(client_fd,sendMsg,strlen(sendMsg));

	}
	else {
		strcpy(sendMsg,"HTTP/1.1 404 NOT FOUND\r\nContent-Type: application/json\r\n\r\n");
 		write(client_fd,sendMsg,strlen(sendMsg));
  		printf("[damda] Error :404 NOT FOUND\n");
	}
    free(sendMsg);
    memset(sendMsg, 0x00, sizeof(sendMsg));
}
*/
int getSid(char* message,char* mSid){
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	
	
	char* sid = (char*)json_string_value(json_object_get(messageJson,"sid"));
	
	char copySid[100];
	if(sid != NULL){
		strcpy(mSid,sid);
												
		json_decref(messageJson);
		
		
		return 1;
	} 
	else {
		json_decref(messageJson);
		return 0;
	}
	
}

/*
 *brief : get seq of response
 *param(char*) : message
 *return : seq
 */
int getSeq(char* message,int* mSeq){
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	int seqInteger;
	
	
	char* seq = (char*)json_string_value(json_object_get(messageJson,"seq"));
	
	if(seq!= NULL){
		
		*mSeq = atoi(seq);
		json_decref(messageJson);
		
		
		return *mSeq;
	} 
	else {
		json_decref(messageJson);
		return -1;
	}
}

/*
 *brief : get operation of response
 *param(char*) : message
 *return : operation
 */
int getOperation(char* message,char* operation){
        json_error_t jsonError;
        json_t* messageJson;
        json_t* jsonMsg;

		messageJson =  json_loads(message,0,&jsonError);
        jsonMsg = json_object_get(messageJson,"msg");
	char* o = (char*)json_string_value(json_object_get(jsonMsg,"o"));
	if(o!=NULL){
		
		strcpy(operation,o);
		json_decref(messageJson);
		return 1;
	} 
	else {
		json_decref(messageJson);
		return 0;
	}
	
}

/*
 *brief : get returnCode of reponse
 *param(char*) : message
 *return : returnCode
 */
int getReturnCode(char* message,int* mReturnCode){
        json_error_t jsonError;
        json_t* messageJson = json_loads(message,0,&jsonError);
        json_t* jsonMsg = json_object_get(messageJson,"msg");
	
		
		
    char* rc = (char*)json_string_value(json_object_get(jsonMsg,"rc")); 
	
	if(rc != NULL){
		
		
		*mReturnCode = atoi(rc);			
		json_decref(messageJson);
		
		
		return *mReturnCode;
	} else {
		json_decref(messageJson);
		return -1;
	}
	
}

/*
 *brief : get errorCode of reponse
 *param(char*) : message
 *return : errorCode
 */
int getErrorCode(char* message,int* mErrorCode){
        json_error_t jsonError;
        json_t* messageJson = json_loads(message,0,&jsonError);
        json_t* jsonMsg = json_object_get(messageJson,"msg");
        
	
    char* ec = (char*)json_string_value(json_object_get(jsonMsg,"ec"));
	if(ec != NULL){
		
		*mErrorCode = atoi(ec);
		json_decref(messageJson);
	
		return *mErrorCode;
	} else {
		json_decref(messageJson);
		return -1;
	}
	
}

/*
 *brief : get resourceUri of reponse
 *param(char*) : message
 *param(size_t) : index
 *return : resourceUri
 */

int getResourceUri(char* message,size_t index,char* mResourceUri){
        json_error_t jsonError;
        json_t* messageJson;
        json_t* jsonMsg; 
        json_t* eArray;
        json_t* eJsonObject;
	

    messageJson = json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	eArray = json_object_get(jsonMsg,"e");
	eJsonObject = json_array_get(eArray,index);
    char* n = (char*)json_string_value(json_object_get(eJsonObject,"n"));
	if(n != NULL){
		strcpy(mResourceUri,n);
		json_decref(messageJson);
		
		return 1;
	}
	else {
		json_decref(messageJson);
		return 0;
	}
}
/*
 *brief : get entity array size of reponse
 *param(char *) : message
 *return : entity array size
 */

int getNArraySize(char* message){
        json_error_t jsonError;
        json_t* messageJson;
        json_t* jsonMsg;
        json_t* eArray;
	size_t jsonArraySize;
	
	int i=0;
        messageJson = json_loads(message,0,&jsonError);
        jsonMsg = json_object_get(messageJson,"msg");
        eArray = json_object_get(jsonMsg,"e");
	jsonArraySize = json_array_size(eArray);
	json_decref(messageJson);
		
	return jsonArraySize;
}


/*
 *brief : get stringValue of reponse
 *param(char *) : message
 *param(size_t) : index
 *return : stringValue
 */


int getStringValue(char* message,size_t index,char* mStringValue){
        json_error_t jsonError;
        json_t* messageJson ;
        json_t* jsonMsg ;
        json_t* eArray ;
        json_t* eJsonObject ;
		
	messageJson = json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	eArray = json_object_get(jsonMsg,"e");
	eJsonObject = json_array_get(eArray,index);
    char* sv = (char*)json_string_value(json_object_get(eJsonObject,"sv"));
	if(sv != NULL){
	
		strcpy(mStringValue,sv);
		
		json_decref(messageJson);
		
		return 1;
	} else {
		json_decref(messageJson);
		return 0;
	}
}

/*
 *brief : get time of response
 *param(char *) : message
 *param(size_t) : index
 *return : time
 */


int getTime(char* message,size_t index,char* mTime){
        json_error_t jsonError;
        json_t* messageJson = json_loads(message,0,&jsonError);
        json_t* jsonMsg = json_object_get(messageJson,"msg");
        json_t* eArray = json_object_get(jsonMsg,"e");
        json_t* eJsonObject = json_array_get(eArray,index);
	
        char* ti = (char*)json_string_value(json_object_get(eJsonObject,"ti"));
	if(ti != NULL){
				
		strcpy(mTime,ti);
		json_decref(messageJson);
		
		return 1;
	} else {
		json_decref(messageJson);
		return 0;
	}
        //return ti;
}

/*
 *brief : get serviceId of response
 *param(char*) : topic
 *return : serviceId
 */
char* getServiceId(char* topic){
	char tempTopicService[100];
	strcpy(tempTopicService,topic);
	char* ptrServiceId = strtok(tempTopicService,"/");
	return ptrServiceId;
}

/*
 *brief : check firmwareUpdateMessage
 *param(char*) : message
 *return : firmwareUpdateMessage 1, not firmwareUpdateMessage 0
 */
int isFirmwareUpdateMessage(char* message){
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	json_t* eArray = json_object_get(jsonMsg,"e");
	json_t* eJsonObject;
	int jsonArraySize = json_array_size(eArray);
	int i = 0;
	int updateNCount = 0;
	char nValue[15];
	if(jsonArraySize == 0){
		json_decref(messageJson);
		return 0;
	}
	for(i=0; i<jsonArraySize; i++){
		eJsonObject = json_array_get(eArray,i);
		strcpy(nValue,json_string_value(json_object_get(eJsonObject,"n")));
		if(strcmp(nValue,"/3/0/3") == 0 || strcmp(nValue,"/5/0/1") == 0){
			updateNCount++;
		}
	}
	json_decref(messageJson);
	if(updateNCount == 2){
		return 1;
	} else {
		return 0;
	}
}

/*
 *brief : get stringValue of reponse by resourceUri
 *param(char*) : message
 *param(char*) : n
 *return : stringValue
 */
int getStringValueByResourceUri(char* message,char* n,char* mStringValue){

	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	json_t* eArray = json_object_get(jsonMsg,"e");
	json_t* eJsonObject;
		

	

	int jsonArraySize = json_array_size(eArray);
	int i=0;
	char nValue[15];
	if(jsonArraySize==0){
		json_decref(messageJson);
		return NULL;
	}
	for(i=0;i<jsonArraySize;i++){
		eJsonObject = json_array_get(eArray,i);
		strcpy(nValue,json_string_value(json_object_get(eJsonObject,"n")));
		if(strcmp(nValue,n)==0){
			char* sv = (char*)json_string_value(json_object_get(eJsonObject,"sv"));
			if(sv != NULL){
				
				strcpy(mStringValue,sv);
				json_decref(messageJson);
				return 1;
			} else {
				json_decref(messageJson);
				return 0;
			}
		}
	}
	json_decref(messageJson);
	return 0;
}

/*
 *brief : get messageType of response
 *param(char*) : topic
 *return : messageType
 */
void getMsgType(char* topic,char* mMsgType){
	int i=0;
	char tempTopicMsgType[100];
	strcpy(tempTopicMsgType,topic);
	
	char* ptrMsgType = strtok(tempTopicMsgType,"/");

	
	while(ptrMsgType!=NULL){
		
		ptrMsgType = strtok(NULL,"/");
		if(i == 0){
			break;
		}
		i++;
	}
	
	strcpy(mMsgType,ptrMsgType);
	//free(ptrMsgType);	
	
}

/*
 *brief : get originId of response
 *param(char*) : topic
 *return : originId
 */
void getOriginId(char* topic,char* mOriginId){
        int i = 0;
        char tempTopicOriginId[100];
        strcpy(tempTopicOriginId,topic);
        
		char* ptrOriginId = strtok(tempTopicOriginId,"/");
                
        while(ptrOriginId != NULL){

				ptrOriginId = strtok(NULL,"/");
                
                if(i==1){
                        break;
                }
                i++;
        }
		strcpy(mOriginId,ptrOriginId);
        
}

/*
 *brief : get targetId of response
 *param(char*) : topic
 *return : targetId
 */
void getTargetId(char* topic,char* mTargetId){
        int i=0;
        char tempTopicTargetId[100];
        strcpy(tempTopicTargetId,topic);

		char* ptrTargetId = strtok(tempTopicTargetId,"/");
        
        
        while(ptrTargetId!=NULL){

               ptrTargetId = strtok(NULL,"/");
                if(i==2){
                        break;
                }
                i++;
        }
		strcpy(mTargetId,ptrTargetId);
    
}
/*
 *brief : get topic operation of response
 *param(char*) : topic
 *return topic operation
 */
void getTopicOprId(char* topic,char* mTopicOprId){
        int i=0;
        char tempTopicOperationId[100];
        strcpy(tempTopicOperationId,topic);
		char* ptrOperationId = strtok(tempTopicOperationId,"/");
    
               
        while(ptrOperationId!=NULL){

				ptrOperationId = strtok(NULL,"/");
			
                
                if(i==3){
                        break;
				}
                i++;
		}
		strcpy(mTopicOprId,ptrOperationId);
        
}

/*
void damda_httpServerInit(int port){

  if((server_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
    {
        Serial.println("[damda] Error : HttpServer - Can't open stream socket\n");
        return;
    }
    memset(&server_addr, 0x00, sizeof(server_addr));


    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);


    if(bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) <0)
    {
        Serial.println("[damda] Error : HttpServer - Can't bind local address.\n");
        return;
    }

    if(listen(server_fd, 5) < 0)
    {
        Serial.println("[damda] Error : HttpServer - Can't listening connect.\n");
        return;
    }

    memset(buffer, 0x00, sizeof(buffer));
    Serial.println("[damda] Info : HttpServer - wating connection request.\n");
    len = sizeof(client_addr);


    Serial.println("[damda] Info : serverSocket Open\n");
}

void* httpReceive(){
char temp[20];
char* header;
char* body;
    Serial.println("[damda] Info : HTTP Server is ready \n");
    while(1)
    {
    client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
        if(client_fd < 0)
        {
            Serial.println("[damda] Error : HttpServer- accept failed.\n");
            return ;
        }
        inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));

    	if((msg_size = read(client_fd,buffer,1024))!=0){

            body = strstr(buffer,"\r\n\r\n");
            header = httpParser(buffer,body,"");

            httpResponseController(header,body);

			 memset(body, 0x00, sizeof(body));
			 memset(header, 0x00, sizeof(header));
            memset(buffer, 0x00, sizeof(buffer));
          close(client_fd);


         }
    }
}

void damda_httpServerThreadStart(){
	httpThread_id = pthread_create(&httpThread,NULL,httpReceive,NULL);

}
*/
int getMessageType(char* topic,char* message){
	
	char operation[10];
	char msgType[10];
	getOperation(message,operation);
	getMsgType(topic,msgType);
	if(isFirmwareUpdateMessage(message)){
		Serial.print("UpdateFileUrl0 : ");
		return MESSAGE_TYPE_FIRMWARE_DOWNLOAD;
	}
	else if(strcmp(operation,"res")==0){
		Serial.print("UpdateFileUrl1 : ");
		
		return MESSAGE_TYPE_RESPONSE;
	}
	else if(strcmp(operation,"r")==0){
		if(strcmp(msgType,"sync")==0){
			Serial.print("UpdateFileUrl2 : ");
			return MESSAGE_TYPE_READ;
		} 
		else if(strcmp(msgType,"async")==0){
			Serial.print("UpdateFileUrl3 : ");
			return MESSAGE_TYPE_READ_ASYNC;
		}
	}
	else if(strcmp(operation,"w")==0){
		if(strcmp(msgType,"sync")==0){
			Serial.print("UpdateFileUrl4 : ");
			return MESSAGE_TYPE_WRITE;
		} else if(strcmp(msgType,"async")==0){
			Serial.print("UpdateFileUrl5 : ");
			return MESSAGE_TYPE_WRITE_ASYNC;
		}
	}
	else if(strcmp(operation,"e")==0){
		if(strcmp(msgType,"sync")==0){
			Serial.print("UpdateFileUrl6 : ");
			return MESSAGE_TYPE_EXECUTE;
		} 
		else if(strcmp(msgType,"async")==0){
			Serial.print("UpdateFileUrl7 : ");
			return MESSAGE_TYPE_EXECUTE_ASYNC;
		}

	}
	else if(strcmp(operation,"d")==0 && strcmp(msgType,"sync") == 0){
		Serial.print("UpdateFileUrl8 : ");
		return MESSAGE_TYPE_DELETE_SYNC;
    }
    else if(strcmp(operation,"d")==0 && strcmp(msgType,"async") == 0){
    	Serial.print("UpdateFileUrl9 : ");
		return MESSAGE_TYPE_DELETE_ASYNC;
	}
}

/*
void damda_setDeviceType(char* myDeviceType){
	strcpy(deviceType,myDeviceType);
}
*/

void messageReceived(MQTTClient *client, char topic[], char payloads[], int length) {
	
	char* payload;
	char* topicName = topic;
	int payloadlen = length;
	int messagelen = strlen(payloads);
	const int arr_size= sizeof(char)*payloadlen+sizeof(char);
	char payloadptr[arr_size];
	int nameIdx = 0;
	int jsonArraySize = 0;
	int svIdx = 0;
	int tiIdx = 0;
	int msgType = 0;
	int nSize = 0;
	int svSize = 0;
	int tiSize = 0;
	int freeIndex = 0;
	int urllen = 0;
	int firmwareVersionlen = 0;

	char sid[100];
	int returnCode = 0;
	int errorCode = 0;
	int seq = -1;
	char firmwareDownloadUrl[256];
	char firmwareVersion[15];
	char operationId[20]; 
	char resourceUri[5][20];
	char stringValue[5][50];
	char receiveTime[5][20];
	payload = payloads;
	Serial.println("seq : 0");
    if(messagelen != payloadlen){
    	if(payloadlen != NULL){
			strncpy(payloadptr,payload,payloadlen);
		}
		payloadptr[payloadlen] = '\0';
    } 
    else {
    	if(payload != NULL){
			strcpy(payloadptr,payload);
		}
    }
    Serial.println("seq : 1");
    
	ResponseMessageInfo msgInfo;
	memset(&msgInfo,0x00,sizeof(msgInfo));	

	getSid(payloadptr,sid);
	strcpy(msgInfo.sid,sid);
	Serial.println("seq : 2");	
	//strcpy(msgInfo.Operation,getOperation(payloadptr));
		
	#ifdef _DEBUG_
	Serial.println(""); 
	Serial.print("Response : ");
	Serial.println(payloadptr); 
	/*Serial.print("msgInfo.sid : ");
	Serial.println(msgInfo.sid); 
	
	Serial.print("msgInfo.seq : ");*/
	#endif
	getReturnCode(payloadptr,&returnCode);
	if(returnCode){
		msgInfo.returnCode = returnCode;
	}
	getErrorCode(payloadptr,&errorCode);
	if(errorCode){
		msgInfo.errorCode =  errorCode;
	}
	getSeq(payloadptr,&seq);
	if(seq != -1){
		msgInfo.seq = seq;
	}
	Serial.println("seq : 3");	
	
	#ifdef _DEBUG_
	/*Serial.print("getReturnCode(payloadptr) : ");
	Serial.println(msgInfo.returnCode);
	Serial.print("getErrorCode(payloadptr) : ");
	Serial.println(msgInfo.errorCode);*/
	#endif
	
	jsonArraySize = getNArraySize(payloadptr);
	msgInfo.responseArraySize = jsonArraySize;
	
	#ifdef _DEBUG_
	/*Serial.print("msgInfo.responseArraySize : ");
	Serial.println(msgInfo.responseArraySize);*/
	#endif
	
	Serial.println("seq : 4");	
	if(jsonArraySize > 0){
		for(nameIdx = 0; nameIdx < jsonArraySize;nameIdx++){
			getResourceUri(payloadptr,nameIdx,resourceUri[nameIdx]);
			nSize = strlen(resourceUri[nameIdx]);
			

			msgInfo.resourceUri[nameIdx] =(char*) malloc(sizeof(char)*nSize+1);
			strcpy(msgInfo.resourceUri[nameIdx],resourceUri[nameIdx]);
			
		}
		if(getStringValue(payloadptr,0,stringValue[0])!=0){
			for(svIdx = 0; svIdx<jsonArraySize; svIdx++){
				getStringValue(payloadptr,svIdx,stringValue[svIdx]);
				svSize = strlen(stringValue[svIdx]);


				msgInfo.stringValue[svIdx] = (char*)malloc(sizeof(char)*svSize+1);
				strcpy(msgInfo.stringValue[svIdx],stringValue[svIdx]);
			}
		}
		if(getTime(payloadptr,0,receiveTime[0])!=0){
			for(tiIdx = 0; tiIdx<jsonArraySize; tiIdx++){
				getTime(payloadptr,tiIdx,receiveTime[tiIdx]);
				tiSize = strlen(receiveTime[tiIdx]);
				
				msgInfo.time[tiIdx] = (char*)malloc(sizeof(char)*tiSize+1);
				strcpy(msgInfo.time[tiIdx],receiveTime[tiIdx]);
			}
		}
	}
	 	Serial.println("seq : 5");	
	msgType = getMessageType(topicName,payloadptr);
		
	if(msgType == MESSAGE_TYPE_DELETE_SYNC){
		damda_Rsp_RemoteDelDevice(msgInfo.sid,"200");
		damda_Req_DeleteDevice();
	} else if(msgType == MESSAGE_TYPE_DELETE_ASYNC){
		damda_Req_DeleteDevice();
	}
	if(msgType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
		getTopicOprId(topicName,operationId);
		if(strcmp(operationId,"write") == 0){
			damda_Rsp_RemoteControl_noEntity(MESSAGE_TYPE_WRITE,sid,"200");
		}
		getStringValueByResourceUri(payloadptr,"/5/0/1",firmwareDownloadUrl);
		urllen = strlen(firmwareDownloadUrl);
		msgInfo.firmwareDownloadUrl = (char*)malloc(urllen+1);
		strcpy(msgInfo.firmwareDownloadUrl,firmwareDownloadUrl);
		
		getStringValueByResourceUri(payloadptr,"/3/0/3",firmwareVersion);
		firmwareVersionlen = strlen(firmwareVersion);
		msgInfo.firmwareVersion = (char*)malloc(firmwareVersionlen+1);
		strcpy(msgInfo.firmwareVersion,firmwareVersion);
	}
	msgInfo.responseType = msgType;
	
	if(messageArrivedCallbackStatus == 1){
		callbackFunc(&msgInfo);
	}
	Serial.println("seq : 6");	
	for(freeIndex=0;freeIndex<jsonArraySize;freeIndex++){
		free(msgInfo.resourceUri[freeIndex]);
		
		if(getStringValue(payloadptr,0,stringValue[0])!=0){
			free(msgInfo.stringValue[freeIndex]);
			
		}
		if(getTime(payloadptr,0,receiveTime[0])!=0){
			free(msgInfo.time[freeIndex]);
			
		}
	}
	Serial.println("seq : 7");	
	if(msgType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
		free(msgInfo.firmwareDownloadUrl);
		free(msgInfo.firmwareVersion);
	}
	Serial.println("seq : 8");	
}

void damda_messageReceived_callback_loop(){
	client.loop();
}

void damda_net_setInsecure(){
	net.setInsecure();
}

void damda_MQTT_Connect(char* mqttUser,char* mqttPassword,char* mqttAddress,int mqttPort,char* device_type)
{
	strcpy(damda_deviceType,device_type);
	char subscribeTopic[100];
  	//damda_Read_EEPROM();

  	//net.setInsecure();      							//SSL보안 적용

//	mqttUser = Topic_Device_id.c_str();					//server_ssid.c_str();							//String to const char* casting
//	mqttPassword = damda_connInfo.authKey;				//server_password.c_str();						//String to const char* casting
//	mqttaddress = damda_connInfo.serverAddr;			//String to const char* casting
//	mqttport = atoi(damda_connInfo.serverPort);			//String to int casting

	/******MQTT 연결 *************************************/
	client.setOptions(120, true, 1000);
	//client.begin("test-iot.godamda.kr",11884,net);
	#ifdef ESP8266
		net.setInsecure();
		client.begin(mqttAddress,mqttPort,net);		
	#else
		client.begin(mqttAddress,mqttPort,net);									//Mqtt client 시작
	#endif
	Serial.println(mqttAddress);
	Serial.println(mqttPort);
	Serial.println(mqttUser);
	Serial.println(mqttPassword);
	//client.onMessage(messageReceived);                            		//CallBack Start  string langth 없이
	client.onMessageAdvanced(messageReceived);                            	//CallBack Start  string langth 있음

	//Serial.print("\nMQTT_connecting...");
	while (!client.connect("Arduino", mqttUser, mqttPassword)) {
	//while (!client.connect(DEVICE_TYPE, mqttUser_buffer, mqttPassword_buffer)) {
		Serial.print(".");
		//damda_Reset_EEPROM();
		//server.handleClient();
		//serverSecure.handleClient();				//Web 명령어 POST / GET 모티링
		delay(500);
	}

  // must be change topic

	//client.subscribe(DEVICE_Service+"/sync/" +Topic_Device_id+ "/iot-server/"+t);
	sprintf(subscribeTopic,"%s/*/iot-server/%s/#",damda_deviceType,mqttUser);
	client.subscribe(subscribeTopic);
	sprintf(subscribeTopic,"%s/*/iot-server/%s/#","SWITCH",mqttUser);
	client.subscribe(subscribeTopic);
	//sprintf(subscribeTopic,"%s/*/%s/iot-server/#",damda_deviceType,mqttUser);
	//client.subscribe(subscribeTopic);
	//client.subscribe("#");


}

void damda_SetConnectInfo(char* server_addr,char* server_port,char* ap_ssid, char* ap_password,char* auth_key)
{
	damda_connInfo.serverAddr = server_addr;
	damda_connInfo.serverPort = server_port;
	damda_connInfo.apSsid = ap_ssid;
	damda_connInfo.apPassword = ap_password;
	damda_connInfo.authKey = auth_key;
}

void damda_setMQTTMsgReceiveCallback(MSG_ARRIVED_CALLBACK_FUNCTION func){
	callbackFunc = func;
	messageArrivedCallbackStatus = 1;
}
