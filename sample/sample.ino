// This example uses an ESP32 Development Board

// https://github.com/256dpi/arduino-mqtt
#include <EEPROM.h>
//#include <WiFiClientSecure.h>
//#include <MQTT.h>
#include <FS.h>
#include <SPIFFS.h>
#include <WebServer.h>
#include <HTTPUpdate.h>
//#include <ESPmDNS.h>
#include <HTTPClient.h>
#include "damdaCoreSDK.h"
#include <ArduinoJson.h>
#include <WiFi.h>
//#include <SoftwareSerial.h>
#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"


//#define _HARD_CODE_
//#define  	DEVICE_SWITCH
#define  	DEVICE_PLUG
#define		ESP32

#define _DEBUG_

const int LED_BUILTIN9 = 2;
const int BUTTON_PIN = 0;
const int BUTTON_PIN1 = 5;
const int BUTTON_LED1 = 16;
const int BUTTON_LED2 = 14;
const int BUTTON_LED3 = 12;
const int BUTTON_LED4 = 13;
const int DIMMING_LED = 15;
//SoftwareSerial T_Serial(4,0); //RX, TX

const char ssid_wifi[] = 			"kofach2g";
const char pass_wifi[] = 			"Kofach1115##";

const char* mqttUser_buffer = 		"WINIA-WN_PLUG-15001801";
const char* mqttPassword_buffer = 	"123456789";

const char *ssid = 					"RTOS_ESP32";
const char *password = 				"1234";

#define DEVICE_MANUFACTURER  		"KONO"
#define DEVICE_MODEL  				"SHOU"
#define DEVICE_SERIAL  				"16003902"
#define DEVICE_TYPE  				"Digital_Sign_Display"
 
String DEVICE_MANUFACTURER_BUFFER 	= DEVICE_MANUFACTURER;
String DEVICE_MODEL_BUFFER 			= DEVICE_MODEL;
String DEVICE_SERIAL_BUFFER 		= DEVICE_SERIAL;
String DEVICE_TYPE_BUFFER 			= DEVICE_TYPE;
String DEVICE_SERVICE_ID 			= "Digital_Sign_Display";


#ifdef DEVICE_SWITCH
char* Power_state = 		"OFF";
char* Power_Led1_state = 	"OFF";
char* Power_Led2_state = 	"OFF";
char* Power_Led3_state = 	"OFF";
char* Dimming_state = 		"16";
#endif

#ifdef DEVICE_PLUG
char* Power_state = 					"OFF";
char* Standby_cutoff_setting_state = 	"false";
char* Standby_cutoff_state = 			"false";
char* Overheat_state = 					"false";
#endif

int EEPROM_SIZE = 254;
int btn_Status = HIGH;

String content;
String st;
int statusCode;

#define SOFT_IPSET_STATIC  	{ 10, 10, 10, 10 	}         //SOFT AP 고정 IP
#define IPSET_STATIC  		{ 192, 168, 2, 82 	}
#define IPSET_GATEWAY 		{ 192, 168, 2, 1   	}
#define IPSET_SUBNET  		{ 255, 255, 255, 0  }
#define IPSET_DNS     		{ 210, 94, 0, 73   	}

byte ip_static[4] 		= {};                               //IP를 받기위한 변수 할당
byte ip_static_AP[] 	= SOFT_IPSET_STATIC;
byte ip_static_bu[] 	= IPSET_STATIC;
byte ip_gateway[] 		= IPSET_GATEWAY;
byte ip_subnet[] 		= IPSET_SUBNET;
byte ip_dns[] 			= IPSET_DNS;

char ucNotify_sync_flag = 0;
char ucNotify_async_flag = 0;

const char * mqttUser = "";
const char * mqttPassword = "";
const char * mqttaddress = "";
int mqttport = 11884;

unsigned long lastMsg = 0;
unsigned long test_para = 1000;      			//1000ms

unsigned long Test_Count = 0;    				//5초
unsigned long Test_Count1 = 0;    				//5초
int Sensor = 0;

char* System_Timer = "1574944039266";
char System_Timer_Set = 0;

// 수정 해야 되는 부분
String server_addr ="";
String server_port = "";
String server_ssid ="";
String server_password ="";
String auth_key ="";

File fsUploadFile;                  			// a File object to temporarily store the received file

String UpdateFileUrl = "http://192.168.2.38:8080/mini.bin";
String Firmware_file_name = "mini.bin";
String UpdateSPIFFSFileName = "http://192.168.2.38:8080/mini.bin";

String Control_o = "";
String Control_n = "";
String Control_ti = "";
String Control_sv = "";
String Control_sid = "";
String Control_did = "";

String Topic_Device_id = DEVICE_MANUFACTURER_BUFFER+"-"+DEVICE_MODEL_BUFFER+"-"+DEVICE_SERIAL_BUFFER;

unsigned char massage_print_flag = 0;

unsigned char Timer_1ms = 0;
unsigned int Timer_10ms = 0;

unsigned long init_t = 0;

String WiFi_SSID = "";
String WiFi_PASS = "";
String WiFi_IP_ADDRESS = "";

char Button_Control = 0;
char Button_Control_Buffer = 0;

DAMDA_connectionInfo damda_sample_connection_info;
DeviceStatusInfo damda_deviceStatusInfo;

#define MAX_TIME 85
#define MAX_DEVICE_INFO_LEN 20
#define MAX_VERSION_LEN 10
#define MAX_FIRMWARE_DOWNLOAD_URL_LEN 512


char firmwareDownloadURL[MAX_FIRMWARE_DOWNLOAD_URL_LEN];
char firmwareVersion[MAX_VERSION_LEN];

unsigned long lastMillis = 0;

WebServer server(18080);

//WiFiClientSecure net;
//MQTTClient client(2048);

void handlebody();

void damda_SPIFFS() 					// Start the SPIFFS and list all contents
{
	
  	if(!SPIFFS.begin(true)){
	  	Serial.println("An Error has occurred while mounting SPIFFS");
	  	return;
	}
	File root = SPIFFS.open("/");

	File file = root.openNextFile();

	while(file){

		Serial.print("FILE: ");
		Serial.println(file.name());
 		Serial.print("Size: ");
		Serial.println(formatBytes(file.size()));

		file = root.openNextFile();
	}
}

void SPIFFS_format(){                       //SPIFFS 포멧
  	Serial.println("SPIFFS format...START");

  	bool formatted = SPIFFS.format();
  	if(formatted){
    	Serial.println("\n\nSuccess formatting");
  	}
  	else{
    	Serial.println("\n\nError formatting");
  	}

  	Serial.println("SPIFFS format...END");
}

void launchWeb(int webtype) {

  	Serial.println("");
  
  	Serial.print("Local IP: ");
  	Serial.println(WiFi.localIP());
  	Serial.print("SoftAP IP: ");
  	Serial.println(WiFi.softAPIP());
  	createWebServer(webtype);
  	server.begin();
  	
  	Serial.println("Server started");
}

void createWebServer(int webtype)
{
  content = "";
  if ( webtype == 1 ) {

    server.on("/", []() {
        IPAddress ip = WiFi.softAPIP();
        String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        content = "<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
        content += ipStr;
        content += "<p>";
        content += st;

        content += "</p><form method='POST' action='setting'><label>server_addr: </label><input name='server_addr' length=32></p>";
        content += "<label>server_port: </label><input name='server_port' length=8></p>";
        content += "<label>server_ssid: </label><input name='server_ssid' length=32></p>";
        content += "<label>server_password: </label><input name='server_password' length=32></p>";
        content += "<label>auth_key: </label><input name='auth_key' length=64></p>";
        content += "<label>wifi_ip: </label><input name='wifi_ip' length=32><input type='submit'></form>";
        content += "</html>";
        server.send(200, "text/html", content);
    });

    server.on("/api/v1/wifi/info/get",HTTP_GET,[](){								//모바일에서 서버 연결 요청  모바일 -> 디바이스
      	server.send(200, "application/json", "{\"manufacturer\":\""+DEVICE_MANUFACTURER_BUFFER+"\",\"model\":\""+DEVICE_MODEL_BUFFER+"\",\"serial\":\""+DEVICE_SERIAL_BUFFER+"\",\"support_type\":\""+DEVICE_TYPE_BUFFER+"\"}");
      	Serial.print("1. send message");
    });


    server.on("/api/v1/wifi/link/start", HTTP_POST, handlebody);			        //모바일에서 서버 연동 시작

	/*server.on("/api/v1/wifi/link/start", HTTP_POST, [](){	       					// if the client requests the upload page     //파일 다운로드
      server.send(200,"application/json","{}"); },                          		// Send status 200 (OK) to tell the client we are ready to receive
      handlebody     																// otherwise, respond with a 404 (Not Found) error
    );*/

    server.on("/api/download", HTTP_POST, damda_firmware_download);				        //서버에서 펌웨어 다운로드

    server.on("/api/firmwareupdate", HTTP_POST, damda_firmware_update);	    	//SPIFFS에서 펌웨어 업데이트

    //server.on("/api/firmwareOTA", HTTP_POST, firmware_OTA);					        //서버에서 펌웨어 업데이트

    server.on("/api/format", HTTP_POST,  SPIFFS_format);					        //SPIFFS 포멧

	server.on("/eeprom", HTTP_POST,  damda_Eeprom_format);					//EEPROM 포멧

	server.on("/api/v1/wifi/link/set", HTTP_POST, damda_WiFi_Reconnect);	    //모바일에서 Wifi 재설정 정보를 보내줌

    server.on("/upload", HTTP_GET, []() {                 							// if the client requests the upload page     //파일 다운로드
      if (!handleFileRead("/"+Firmware_file_name))                   				// send it if it exists
        server.send(404, "text/plain", "404: Not Found"); 							// otherwise, respond with a 404 (Not Found) error
    });

    server.on("/upload", HTTP_POST,                       							// if the client posts to the upload page     //파일 직접 Upload
      [](){ server.send(200); },                          							// Send status 200 (OK) to tell the client we are ready to receive
      handleFileUpload                                    							// Receive and save the file
    );

    server.onNotFound([]() {                              							// If the client requests any URI
      if (!handleFileRead(server.uri()))                  							// send it if it exists
        server.send(404, "text/plain", "404: Not Found"); 							// otherwise, respond with a 404 (Not Found) error
    });

    server.on("/setting", []() {
        server_addr = server.arg("server_addr");
        server_port = server.arg("server_port");
        server_ssid = server.arg("server_ssid");
        server_password = server.arg("server_password");
        auth_key = server.arg("auth_key");
        String WiFi_IP_ADDRESS = server.arg("wifi_ip");
        const char * c = WiFi_IP_ADDRESS.c_str();

        if (server_ssid.length() > 0 && server_password.length() > 0) {
          	Serial.println("clearing eeprom");
          	for (int i = 0; i < EEPROM_SIZE; ++i) {
            	EEPROM.write(i, 0);
          	}
          	EEPROM.commit();

        	Serial.print("writing eeprom ssid:");
		if (server_addr.length() > 0){
	          for (int i = 0; i < server_addr.length(); ++i)
	          {
	            EEPROM.write(i, server_addr[i]);
	            Serial.print("server_addr: ");
	            Serial.println(server_addr[i]);
	          }

	          for (int i = 0; i < server_port.length(); ++i)
	          {
	            EEPROM.write(32+i, server_port[i]);
	            Serial.print("server_port: ");
	            Serial.println(server_port[i]);
	          }

			  for (int i = 0; i < server_ssid.length(); ++i)
	          {
	            EEPROM.write(40+i, server_ssid[i]);
	            Serial.print("server_ssid: ");
	            Serial.println(server_ssid[i]);
	          }

	          for (int i = 0; i < server_password.length(); ++i)
	          {
	            EEPROM.write(72+i, server_password[i]);
	            Serial.print("server_password: ");
	            Serial.println(server_password[i]);
	          }

	          for (int i = 0; i < auth_key.length(); ++i)
	          {
	            EEPROM.write(104+i, auth_key[i]);
	            Serial.print("auth_key: ");
	            Serial.println(auth_key[i]);
	          }
	          for (int i = 0; i < WiFi_IP_ADDRESS.length(); ++i)
			  {
				
				EEPROM.write(140+i, WiFi_IP_ADDRESS[i]);
				Serial.print("Write: ");
				Serial.println(WiFi_IP_ADDRESS[i]);
	          }
	          EEPROM.write(EEPROM_SIZE, 3);
			}
          	EEPROM.commit();

		  	/**** IP 주소값을 0.0.0.0   .단위로 잘라낸다  *******************************/
          	char ipChar[WiFi_IP_ADDRESS.length()+1];
          	WiFi_IP_ADDRESS.toCharArray(ipChar,WiFi_IP_ADDRESS.length()+1);
          	char *p = ipChar;
          	char *str;
          	int index=0;
          	while ((str = strtok_r(p, ".", &p)) != NULL){
            	// delimiter is the semicolon
            	ip_static[index] = byte(atoi(str));
            	index++;
          	}
		 	/*********************************************************************/
          	content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
          	statusCode = 200;
          	Serial.println("Send 200");
        }
        else
        {
          content = "{\"Error\":\"404 not found\"}";
          statusCode = 404;
          Serial.println("Send 404");
        }

        server.send(statusCode, "application/json", content);		//EEPROM 성공 실패 메세지 출력
       
		WiFi.mode(WIFI_AP_STA);
        WiFi.config(IPAddress(ip_static_bu), IPAddress(ip_gateway), IPAddress(ip_subnet), IPAddress(ip_dns));


        Serial.println(WiFi.localIP());
        WiFi.begin(server_ssid.c_str(), server_password.c_str());
        return;
    });

  }

}

void handlebody(){				// 모바일 데이터 송신 및 수신
	
  	server.send(200, "application/json", "{}");
  	Serial.print("2. send message");
  	delay(1000);

  	StaticJsonDocument<300> newBuffer;
  	deserializeJson(newBuffer, server.arg("plain"));                              //  buffer에 body 내용 json으로 변환
  	JsonObject bodyContent = newBuffer.as<JsonObject>();                          //  json 내용 jsonObject에 할당
  	JsonVariant server_addrVariant = bodyContent.getMember("server_addr");        //  server_addr 내용 가져옴
  	server_addr = server_addrVariant.as<const char*>();                    		//  내용 String으로 형변환
  	JsonVariant server_portVariant = bodyContent.getMember("server_port");
  	server_port = server_portVariant.as<const char*>();
  	JsonVariant server_ssidVariant = bodyContent.getMember("ap_ssid");
  	server_ssid = server_ssidVariant.as<const char*>();
  	JsonVariant server_passwordVariant = bodyContent.getMember("ap_password");
  	server_password = server_passwordVariant.as<const char*>();
  	JsonVariant auth_keyVariant = bodyContent.getMember("auth_key");
  	auth_key = auth_keyVariant.as<const char*>();


    damda_SetConnectInfo((char *)server_addr.c_str(),(char *)server_port.c_str(),(char *)server_ssid.c_str(),(char *)server_password.c_str(),(char *)auth_key.c_str());

    damda_sample_connection_info = getConnectionInfo();

    Serial.print("server_addr : ");
  	Serial.println(damda_sample_connection_info.serverAddr);
  	Serial.print("server_port : ");
  	Serial.println(damda_sample_connection_info.serverPort);
  	Serial.print("WiFi_ssid : ");						//와이파이 아이디
  	Serial.println(damda_sample_connection_info.apSsid);
  	Serial.print("WiFi_password : ");				//와이파이 패스워드
  	Serial.println(damda_sample_connection_info.apPassword);
  	Serial.print("auth_key : ");
  	Serial.println(damda_sample_connection_info.authKey);


  	//****************IP ADDRESS 입력 *************************************/
	if (server_ssid.length() > 0){                         //IPADDress EEPROM 입력
		Serial.println("clearing eeprom_IPAddress");
		for (int i = 0; i <= EEPROM_SIZE; ++i) {
			EEPROM.write(i, 0);
		}

	  	for (int i = 0; i < server_addr.length(); ++i)
      	{
        	EEPROM.write(i, server_addr[i]);
        	Serial.print("server_addr: ");
        	Serial.println(server_addr[i]);
      	}

		for (int i = 0; i < server_port.length(); ++i)
		{
        	EEPROM.write(32+i, server_port[i]);
        	Serial.print("server_port: ");
        	Serial.println(server_port[i]);
      	}

	  	for (int i = 0; i < server_ssid.length(); ++i)
      	{
        	EEPROM.write(40+i, server_ssid[i]);
        	Serial.print("server_ssid: ");
        	Serial.println(server_ssid[i]);
        	Serial.print("i : ");
        	Serial.print(i);
      	}

      	for (int i = 0; i < server_password.length(); ++i)
      	{
        	EEPROM.write(72+i, server_password[i]);
        	Serial.print("server_password: ");
        	Serial.println(server_password[i]);
        	Serial.print("i : ");
        	Serial.print(i);
      	}

      	for (int i = 0; i < auth_key.length(); ++i)
      	{
        	EEPROM.write(104+i, auth_key[i]);
        	Serial.print("auth_key: ");
        	Serial.println(auth_key[i]);
        	Serial.print("i : ");
        	Serial.print(i);
      	}
      	for (int i = 0; i < WiFi_IP_ADDRESS.length(); ++i)
	  	{
			//EEPROM.write(136+i, WiFi_IP_ADDRESS[i]);
			EEPROM.write(140+i, WiFi_IP_ADDRESS[i]);
			Serial.print("WiFi_IP_ADDRESS: ");
			Serial.println(WiFi_IP_ADDRESS[i]);
      	}
      	EEPROM.write(EEPROM_SIZE, 3);
  	}
  	EEPROM.commit();
  	delay(100);
  	ESP.restart();
  	return;
}

String getContentType(String filename){
	if(filename.endsWith(".htm")) return "text/html";
	else if(filename.endsWith(".html")) return "text/html";
	else if(filename.endsWith(".css")) return "text/css";
	else if(filename.endsWith(".js")) return "application/javascript";
	else if(filename.endsWith(".png")) return "image/png";
	 else if(filename.endsWith(".gif")) return "image/gif";
	else if(filename.endsWith(".jpg")) return "image/jpeg";
	else if(filename.endsWith(".ico")) return "image/x-icon";
	else if(filename.endsWith(".xml")) return "text/xml";
	else if(filename.endsWith(".pdf")) return "application/x-pdf";
	else if(filename.endsWith(".zip")) return "application/x-zip";
	else if(filename.endsWith(".gz")) return "application/x-gzip";
	else if(filename.endsWith(".bin")) return "application/octet-stream";
	return "text/plain";
}

bool handleFileRead(String path) { 	// send the right file to the client (if it exists)
	
  	server.send(200,"OK");
  	Serial.println("handleFileRead: " + path);
  	if (path.endsWith("/")) path += Firmware_file_name;			// If a folder is requested, send the index file
  	String contentType = getContentType(path);             		// Get the MIME type
  	String pathWithGz = path + ".gz";
  	if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) { 	// If the file exists, either as a compressed archive, or normal
	    if (SPIFFS.exists(pathWithGz))                         	// If there's a compressed version available
		path += ".gz";                                         	// Use the compressed verion
	    File file = SPIFFS.open(path, "r");                    	// Open the file
	    size_t sent = server.streamFile(file, contentType);    	// Send it to the client
	    file.close();                                          	// Close the file again
	    Serial.println(String("\tSent file: ") + path);
	    return true;
  	}
  	Serial.println(String("\tFile Not Found: ") + path);   		// If the file doesn't exist, return false
  	//client.loop();											//MQTT Call back 모니터링(1,0,1);
  	return false;
}

void handleFileUpload(){ 										// upload a new file to the SPIFFS
	
  	server.send(200,"UPDATE_START");
  	HTTPUpload& upload = server.upload();
  	if(upload.status == UPLOAD_FILE_START){
    	String filename = upload.filename;
    	if(!filename.startsWith("/")) filename = "/"+filename;
    	Serial.print("handleFileUpload Name: "); Serial.println(filename);
    	fsUploadFile = SPIFFS.open(filename, "w");            	// Open the file for writing in SPIFFS (create if it doesn't exist)
    	filename = String();
  	}
  	else if(upload.status == UPLOAD_FILE_WRITE){
    	if(fsUploadFile)
      	fsUploadFile.write(upload.buf, upload.currentSize); 	// Write the received bytes to the file
  	}
  	else if(upload.status == UPLOAD_FILE_END){
    	if(fsUploadFile) {                                    	// If the file was successfully created
      		fsUploadFile.close();                               // Close the file again
      		Serial.print("handleFileUpload Size: "); Serial.println(upload.totalSize);
      		server.sendHeader("Location","/"+Firmware_file_name);		// Redirect the client to the success page
      		server.send(303);
    	}
    	else {
      	server.send(500, "text/plain", "500: couldn't create file");
    	}
  	}
  	server.send(200,"OK");
  	
}

String formatBytes(size_t bytes) 										// convert sizes in bytes to KB and MB
{
	if (bytes < 1024) {
		return String(bytes) + "B";
	}
	else if (bytes < (1024 * 1024)) {
    	return String(bytes / 1024.0) + "KB";
	}
	else if (bytes < (1024 * 1024 * 1024)) {
		return String(bytes / 1024.0 / 1024.0) + "MB";
  	}
}

void damda_firmware_download(){               			//WEB SERVER로 부터 파일을 다운로드 하여, SPIFFS에 저장
	
  	server.send(200,"OK");
  	HTTPClient http;

  	String response = "";
  	String url = String(UpdateFileUrl) + response.substring(4);
    //url = http://123.123.123.123/SPIFFS/test2.htm
    String file_name = response.substring(response.lastIndexOf('/'));
    //file_name = test2.htm
    Serial.println(url);
    File f = SPIFFS.open("/"+Firmware_file_name, "w");
    if (f) {
      	http.begin(url);
      	int httpCode = http.GET();
      	if (httpCode > 0) {
        	if (httpCode == HTTP_CODE_OK) {
          		http.writeToStream(&f);
        	}
      	}
      	else {
        	Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      	}
      	f.close();
    }
    http.end();
    server.send(200,"Complate");
    
    Serial.println("Firmware Download complate");
	damda_SPIFFS();
    
}
void damda_firmware_update(){           			//SPIFFS에 들어있는 firmware 파일을 직접 Update 한다.
	
  	server.send(200,"OK");
  	
  	File root = SPIFFS.open("/");
  	File file = SPIFFS.open("/"+Firmware_file_name, "r");

  	uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
  	if (!Update.begin(maxSketchSpace, U_FLASH)) { //start with max available size
    	Update.printError(Serial);
    	Serial.println("ERROR");
    	return;
  	}

  	while (file.available()) {
    	uint8_t ibuffer[128];
    	file.read((uint8_t *)ibuffer, 128);
    	Serial.println((char *)ibuffer);
    	Update.write(ibuffer, sizeof(ibuffer));
  	}

  	Serial.print(Update.end(true));
  	
  	file.close();
  	server.send(200,"Complate");

  	ESP.restart();      //펌웨어 업데이트 하고 재시작
}
void damda_Eeprom_format(){
	for (int i = 0; i <= EEPROM_SIZE; ++i)
    {
      EEPROM.write(i, 0);
    }
    EEPROM.commit();
    ESP.restart();
}
void damda_Read_EEPROM()
{
	  // read eeprom for Mqtt_ssid and Mqtt_pass and Mqtt_address and Mqtt_port
	  Serial.println("Reading EEPROM server_ssid,server_password,server_addr,server_port");
	  server_addr = "";
	  for (int i = 0; i < 32; ++i)            //wifi SSID 읽어 오기
	  {
	    server_addr += char(EEPROM.read(i));
	  }
	  Serial.print("server_addr: ");
	  Serial.println(server_addr);

	  server_port = "";
	  for (int i = 32; i < 40; ++i)            //wifi PASSWORD 읽어 오기
	  {
	    server_port += char(EEPROM.read(i));
	  }
	  Serial.print("server_port: ");
	  Serial.println(server_port);

	  server_ssid = "";
	  for (int i = 40; i < 72; ++i)            //Server ADDRESS 읽어 오기
	  {
	    server_ssid += char(EEPROM.read(i));
	  }
	  Serial.print("server_ssid: ");
	  Serial.println(server_ssid);

	  server_password = "";
	  for (int i = 72; i < 104; ++i)            //Server PORT 읽어 오기
	  {
	    server_password += char(EEPROM.read(i));
	  }
	  Serial.print("server_password: ");
	  Serial.println(server_password);

	  auth_key = "";
	  //for (int i = 104; i < 136; ++i)            //Server PORT 읽어 오기
	  for (int i = 104; i < 140; ++i)            //Server PORT 읽어 오기
	  {
	    auth_key += char(EEPROM.read(i));
	  }
	  Serial.print("auth_key: ");
	  Serial.println(auth_key);

	  WiFi_IP_ADDRESS = "";
	  //for (int i = 136; i < 168; ++i)            //Server PORT 읽어 오기
	  for (int i = 140; i < 172; ++i)            //Server PORT 읽어 오기
	  {
	    WiFi_IP_ADDRESS += char(EEPROM.read(i));
	  }
	  Serial.print("WiFi_IP_ADDRESS: ");
	  Serial.println(WiFi_IP_ADDRESS);

		damda_SetConnectInfo((char *)server_addr.c_str(),(char *)server_port.c_str(),(char *)server_ssid.c_str(),(char *)server_password.c_str(),(char *)auth_key.c_str());
		damda_sample_connection_info.serverAddr = server_addr.c_str();
		damda_sample_connection_info.serverPort = server_port.c_str();
		damda_sample_connection_info.apSsid = server_ssid.c_str();
		damda_sample_connection_info.apPassword = server_password.c_str();
		damda_sample_connection_info.authKey = auth_key.c_str();

	  	#ifdef _DEBUG_
		  	Serial.print("serverAddr : ");
			Serial.println((const __FlashStringHelper *)damda_sample_connection_info.serverAddr);
			Serial.print("serverPort : ");
			Serial.println((const __FlashStringHelper *)damda_sample_connection_info.serverPort);
			Serial.print("apSsid : ");
	   	 	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.apSsid);
	    	Serial.print("apPassword : ");
	    	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.apPassword);
	    	Serial.print("authKey : ");
	    	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.authKey);
	    	Serial.print("WiFi_IP_ADDRESS : ");
	    	Serial.println(WiFi_IP_ADDRESS);
    	#endif
    	
		char ipChar[WiFi_IP_ADDRESS.length()+1];
		WiFi_IP_ADDRESS.toCharArray(ipChar,WiFi_IP_ADDRESS.length()+1);
		char *p = ipChar;
		char *str;
		int index=0;
		while ((str = strtok_r(p, ".", &p)) != NULL){
			ip_static[index] = byte(atoi(str));
			index++;
		}
}
void damda_Reset_EEPROM(){						//EEPROM 초기화
  	//버튼을이용한 RESET
	btn_Status = digitalRead (BUTTON_PIN);
  	if(btn_Status == LOW){
	  	Serial.println("Deregister Device");
		
	  	damda_Req_DeregisterDevice();
	
	  	delay(1000);	
	
	    Serial.print("EEPROM RESET ");
	    WiFi.disconnect(true);
	
	    digitalWrite(LED_BUILTIN9, HIGH);
	
	    for (int i = 0; i <= EEPROM_SIZE; ++i)
	    {
	      EEPROM.write(i, 0);
	    }
	    EEPROM.commit();
	    ESP.restart();
	    delay(500);
  	}
  	else{
    	digitalWrite(LED_BUILTIN9, LOW);
  	}
}

bool damda_Wifi_check(void) {
	int c = 0;
	launchWeb(1);
	Serial.println("Waiting for Wifi to connect");
	//while ( c < 60000 ) {
	while (1) {
		damda_Reset_EEPROM();
		server.handleClient();
    	if (WiFi.status() == WL_CONNECTED) {
    		Serial.print("Local IP: ");
  			Serial.println(WiFi.localIP());
    		return true;
    	}

	    c++;
	    if(c > 20000){
	    	c = 0;
	    	Serial.print(WiFi.status());
	    }
	}
  	Serial.println("Wifi to Disconnected!");
  	return false;
}
void damda_WiFi_Connect()
{
	damda_sample_connection_info = getConnectionInfo();

	if(EEPROM.read(EEPROM_SIZE) == 2){
		Serial.println("--------------------SOFT STATION MODE START-------------------");
		WiFi.mode(WIFI_STA);
		//WiFi.config(IPAddress(ip_static_bu), IPAddress(ip_gateway), IPAddress(ip_subnet), IPAddress(ip_dns));
		WiFi.begin(damda_sample_connection_info.apSsid,damda_sample_connection_info.apPassword);			//Wifi STARTION MODE 시작

	}
	else if(EEPROM.read(EEPROM_SIZE) == 3){
    	//WIFI AP_STATION모드 시작
    	Serial.println("-------------------SOFT AP + STATION MODE START-----------------");
    	Serial.print("Wifi ssid : ");
    	Serial.println(damda_sample_connection_info.apSsid);
    	Serial.print("Wifi pass : ");
    	Serial.println(damda_sample_connection_info.apPassword);
    	WiFi.mode(WIFI_AP_STA);
    	//WiFi.mode(WIFI_STA);
    	delay(500);
	  	WiFi.softAPConfig(IPAddress(SOFT_IPSET_STATIC),IPAddress(IPSET_GATEWAY),IPAddress(IPSET_SUBNET));
	  	WiFi.softAP(ssid,password);							//Wifi AP MODE 시작
	  	delay(500);
	  	//WiFi.config(IPAddress(ip_static_bu), IPAddress(ip_gateway), IPAddress(ip_subnet), IPAddress(ip_dns));
		WiFi.begin(damda_sample_connection_info.apSsid,damda_sample_connection_info.apPassword);			//Wifi STARTION MODE 시작
		

	}
    else{
    	//WIFI AP_STATION모드 시작
    	Serial.println("-------------------SOFT AP MODE START-----------------");
    	WiFi.mode(WIFI_AP);
    	delay(500);
	  	WiFi.softAPConfig(IPAddress(SOFT_IPSET_STATIC),IPAddress(IPSET_GATEWAY),IPAddress(IPSET_SUBNET));
	  	delay(500);
	  	WiFi.softAP(ssid,password);							//Wifi AP MODE 시작
	}
	
	String data = damda_sample_connection_info.apSsid;
	if(data.length() > 1){
		WiFi.begin(damda_sample_connection_info.apSsid, damda_sample_connection_info.apPassword);
		if (damda_Wifi_check()) {
			delay(500);
			return;
		}

		Serial.print("Local IP: ");
		Serial.println(WiFi.localIP());
		Serial.print("SoftAP IP: ");
		Serial.println(WiFi.softAPIP());
	}
	else{
	    int n = WiFi.scanNetworks();
	    Serial.println("scan done");
	    if (n == 0)
			Serial.println("no networks found");
	    else
	    {
			Serial.print(n);
			Serial.println(" networks found");
	      	for (int i = 0; i < n; ++i)
	      	{
		        // Print SSID and RSSI for each network found
		        Serial.print(i + 1);
	            Serial.print(": ");
	            Serial.print(WiFi.SSID(i));
	            Serial.print(" (");
	            Serial.print(WiFi.RSSI(i));
	            Serial.print(")");
	            Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");
	            delay(10);
	      	}
	    }
	    st = "<ol>";
	    for (int i = 0; i < n; ++i)
	    {
	        // Print SSID and RSSI for each network found
	        st += "<li>";
	        st += WiFi.SSID(i);
	        st += " (";
	        st += WiFi.RSSI(i);
	        st += ")";
	        st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
	        st += "</li>";
	    }
	    st += "</ol>";
	    delay(100);
	    damda_Wifi_check();
	   
	}
}
void damda_WiFi_Reconnect(){
	server.send(200, "application/json", "{}");
  	delay(1000);

  	StaticJsonDocument<300> newBuffer;
  	deserializeJson(newBuffer, server.arg("plain"));                              //  buffer에 body 내용 json으로 변환
  	JsonObject bodyContent = newBuffer.as<JsonObject>();                          //  json 내용 jsonObject에 할당

  	JsonVariant server_ssidVariant = bodyContent.getMember("ap_ssid");
  	server_ssid = server_ssidVariant.as<const char*>();
  	JsonVariant server_passwordVariant = bodyContent.getMember("ap_password");
  	server_password = server_passwordVariant.as<const char*>();

	damda_sample_connection_info.apSsid = server_ssid.c_str();
	damda_sample_connection_info.apPassword = server_password.c_str();

  	Serial.print("WiFi_ssid : ");						//와이파이 아이디
  	Serial.println(damda_sample_connection_info.apSsid);
  	Serial.print("WiFi_password : ");				//와이파이 패스워드
  	Serial.println(damda_sample_connection_info.apPassword);
	
	/*for (int i = 40; i < 72; ++i)            //Server ADDRESS 읽어 오기
	{
	    EEPROM.write(40+i, 0);
	}
	for (int i = 72; i < 104; ++i)            //Server PORT 읽어 오기
  	{
    	EEPROM.write(72+i, 0);
  	}*/
	EEPROM.commit();
	Serial.println("EEPROM Erassing Complate");
	delay(100);
	
  	if (server_ssid.length() > 0){
	  	for (int i = 0; i < server_ssid.length(); ++i)
	  	{
		    EEPROM.write(40+i, server_ssid[i]);
		    Serial.print("server_ssid: ");
		    Serial.println(server_ssid[i]);
	  	}

	  	for (int i = 0; i < server_password.length(); ++i)
	  	{
		    EEPROM.write(72+i, server_password[i]);
		    Serial.print("server_password: ");
		    Serial.println(server_password[i]);
	  	}
	}
  	EEPROM.commit();
  	delay(100);
  	ESP.restart();
}

void message_callback(ResponseMessageInfo* ms){
	int i = 0;
	int msgType = 0;
	const int arraySize = ms->responseArraySize;
	#ifdef DEVICE_SWITCH
		char* refrigeratorStringValue[5] = {Power_state,Power_Led1_state,Power_Led2_state,Power_Led3_state,Power_Led3_state};
	#endif
	#ifdef DEVICE_PLUG
		char* refrigeratorStringValue[4] = {Power_state,Standby_cutoff_setting_state,Standby_cutoff_state,Overheat_state};
	#endif
	
	
		#ifdef DEVICE_SWITCH
		
		if(ms->responseType == MESSAGE_TYPE_READ || ms->responseType == MESSAGE_TYPE_WRITE || ms->responseType == MESSAGE_TYPE_EXECUTE || ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){			

			if(strcmp(ms->resourceUri[0],"/3306/0/5853") == 0){
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_state = "ON";
				else if(strcmp(ms->stringValue[0],"OFF") == 0)Power_state = "OFF";
				else Power_state = "OFF";
					Serial.println("-----------Power_state---------");
					Serial.println(Power_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5850") == 0){
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led1_state = "ON";
				else Power_Led1_state = "OFF";
					Serial.println("-----------Power_Led1_state---------");
					Serial.println(Power_Led1_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5851") == 0){
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led2_state = "ON";
				else Power_Led2_state = "OFF";
					Serial.println("-----------Power_Led2_state---------");
					Serial.println(Power_Led2_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5852") == 0){
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led3_state = "ON";
				else Power_Led3_state = "OFF";
					Serial.println("----------Power_Led3_state----------");
					Serial.println(Power_Led3_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3343/0/5548") == 0){
				if(strcmp(ms->stringValue[0],"16") == 0)Dimming_state = "16";
				else if(strcmp(ms->stringValue[0],"42") == 0)Dimming_state = "42";
				else if(strcmp(ms->stringValue[0],"83") == 0)Dimming_state = "83";
				//else if(strcmp(Control_sv.c_str(),"75") == 0)Dimming_state = "75";
				//else if(strcmp(Control_sv.c_str(),"100") == 0)Dimming_state = "100";
				else Dimming_state = "0";
					Serial.println("----------Dimming_state----------");
					Serial.println(Dimming_state);
			}
						
			if(ms->responseType == MESSAGE_TYPE_READ){
				damda_Rsp_RemoteControl(MESSAGE_TYPE_READ,ms->sid,"200",ms->resourceUri,refrigeratorStringValue,arraySize);	
			} 
			else if(ms->responseType == MESSAGE_TYPE_WRITE){	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_WRITE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_EXECUTE){	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_EXECUTE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
				strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
				strcpy(firmwareVersion,ms->firmwareVersion);	
									
			}
		}
		
		if(ms->responseType == MESSAGE_TYPE_DELETE_SYNC){
			
			Serial.println("Soft AP ON");
			Serial.println("Wifi Disconnect");
			Serial.println("EEPROM format");
			
			
			damda_Eeprom_format();
			delay(10);
			EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			//EEPROM.write(EEPROM_SIZE, 2);		// Soft STA ON
			//EEPROM.write(EEPROM_SIZE, 3);		// Soft AP_STA ON
			EEPROM.commit();
			delay(10);
				
			Serial.println("Soft AP MODE START");
			delay(10);
			ESP.restart();
		}
		if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
			strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
			UpdateFileUrl = firmwareDownloadURL;
			
		}
		#endif
		
		#ifdef DEVICE_PLUG
	
		if(ms->responseType == MESSAGE_TYPE_READ || ms->responseType == MESSAGE_TYPE_WRITE || ms->responseType == MESSAGE_TYPE_EXECUTE || ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){				
			if(strcmp(ms->resourceUri[0],"/3312/0/5850") == 0){			
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_state = "ON";
				else if(strcmp(ms->stringValue[0],"OFF") == 0)Power_state = "OFF";
				else Power_state = "OFF";
					Serial.print("Power_state :");
					Serial.println(Power_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3306/0/5850") == 0){
				if(strcmp(ms->stringValue[0],"true") == 0)Standby_cutoff_setting_state = "true";
				else Standby_cutoff_setting_state = "false";
					Serial.print("Standby_cutoff_setting_state :");
					Serial.println(Standby_cutoff_setting_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3342/0/5500") == 0){
				if(strcmp(ms->stringValue[0],"true") == 0)Standby_cutoff_state = "true";
				else Standby_cutoff_state = "false";
					Serial.print("Standby_cutoff_state :");
					Serial.println(Standby_cutoff_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3342/0/5501") == 0){
				if(strcmp(ms->stringValue[0],"true") == 0)Overheat_state = "true";
				else Overheat_state = "false";
					Serial.print("Overheat_state :");
					Serial.println(Overheat_state);
			}
			
			if(ms->responseType == MESSAGE_TYPE_READ){
				damda_Rsp_RemoteControl(MESSAGE_TYPE_READ,ms->sid,"200",ms->resourceUri,refrigeratorStringValue,arraySize);	
			} 
			else if(ms->responseType == MESSAGE_TYPE_WRITE){	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_WRITE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_EXECUTE){	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_EXECUTE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
				strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
				strcpy(firmwareVersion,ms->firmwareVersion);					
			}
		}
		if(ms->responseType == MESSAGE_TYPE_DELETE_SYNC){
			
			Serial.println("Soft AP ON");
			Serial.println("Wifi Disconnect");
			Serial.println("EEPROM format");
			
			damda_Eeprom_format();
			delay(10);
			EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			//EEPROM.write(EEPROM_SIZE, 2);		// Soft STA ON
			//EEPROM.write(EEPROM_SIZE, 3);		// Soft AP_STA ON
			EEPROM.commit();
			delay(10);
			
			Serial.println("Soft AP MODE START");
			delay(10);
			ESP.restart();
		}
		if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
			strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
			UpdateFileUrl = firmwareDownloadURL;
			
		}
		#endif
		
		
		massage_print_flag = 1;
}

void Test_Command(){
	if(massage_print_flag == 1){
		Serial.println("1 : Soft AP ON");
	  	Serial.println("2 : Soft AP OFF");
	  	Serial.println("3 : Mqtt connect");
		Serial.println("4 : Register Device");
		Serial.println("5 : Update Device Information");
		Serial.println("6 : Timesync ON");
		Serial.println("7 : Report periodic information(notify)");
		Serial.println("8 : Report periodic information(notify) STOP");
		Serial.println("9 : Report event notification(notify)");
		Serial.println("10 : Unregister Device");
		Serial.println("11 : Firmware Download init");
		Serial.println("12 : Firmware Download");
		Serial.println("13 : Firmware info update");
		Serial.println("14 : Firmware update");
		Serial.println("");
		massage_print_flag = 0;
	}
}
void LED_Contol(){
	#ifdef DEVICE_SWITCH
	if(Power_state == "ON")digitalWrite(BUTTON_LED1, HIGH);
	else  digitalWrite(BUTTON_LED1, LOW);

	if(Power_Led1_state == "ON")digitalWrite(BUTTON_LED2, HIGH);
	else digitalWrite(BUTTON_LED2, LOW);

	if(Power_Led2_state == "ON")digitalWrite(BUTTON_LED3, HIGH);
	else digitalWrite(BUTTON_LED3, LOW);

	if(Power_Led3_state == "ON")digitalWrite(BUTTON_LED4, HIGH);
	else digitalWrite(BUTTON_LED4, LOW);

	if(Dimming_state == "16")digitalWrite(DIMMING_LED, LOW);
	else digitalWrite(DIMMING_LED, HIGH);
	#endif

	#ifdef DEVICE_PLUG
		
		if(Power_state == "ON")digitalWrite(BUTTON_LED1, HIGH);
		else  digitalWrite(BUTTON_LED1, LOW);
	
		if(Standby_cutoff_setting_state == "ON")digitalWrite(BUTTON_LED2, HIGH);
		else digitalWrite(BUTTON_LED2, LOW);
	
		if(Standby_cutoff_state == "ON")digitalWrite(BUTTON_LED3, HIGH);
		else digitalWrite(BUTTON_LED3, LOW);
	
		if(Overheat_state == "ON")digitalWrite(BUTTON_LED4, HIGH);
		else digitalWrite(BUTTON_LED4, LOW);
	#endif
	
}

void Notify_sync(){
		const char* sid = 0;
		if(ucNotify_sync_flag == 1)sid = "1";
		else if(ucNotify_async_flag == 1) sid = "2";

	#ifdef DEVICE_PLUG
		NotifyParameter DeviceNotifyParameter[4];

		DeviceNotifyParameter[0].resourceUri = "/3312/0/5850";
	    DeviceNotifyParameter[1].resourceUri = "/3306/0/5850";
	    DeviceNotifyParameter[2].resourceUri = "/3342/0/5500";
	    DeviceNotifyParameter[3].resourceUri = "/3342/0/5501";

		DeviceNotifyParameter[0].stringValue = Power_state;
		DeviceNotifyParameter[1].stringValue = Standby_cutoff_setting_state;
		DeviceNotifyParameter[2].stringValue = Standby_cutoff_state;
		DeviceNotifyParameter[3].stringValue = Overheat_state;


		DeviceNotifyParameter[0].time = System_Timer;
		DeviceNotifyParameter[1].time = System_Timer;
		DeviceNotifyParameter[2].time = System_Timer;
		DeviceNotifyParameter[3].time = System_Timer;
		damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,4);

		ucNotify_async_flag = 0;
	#endif
	#ifdef DEVICE_SWITCH
		NotifyParameter DeviceNotifyParameter[5];

		DeviceNotifyParameter[0].resourceUri = "/3306/0/5853";
	    DeviceNotifyParameter[1].resourceUri = "/3312/0/5850";
	    DeviceNotifyParameter[2].resourceUri = "/3312/0/5851";
	    DeviceNotifyParameter[3].resourceUri = "/3312/0/5852";
	    DeviceNotifyParameter[4].resourceUri = "/3343/0/5548";

		DeviceNotifyParameter[0].stringValue = Power_state;
		DeviceNotifyParameter[1].stringValue = Power_Led1_state;
		DeviceNotifyParameter[2].stringValue = Power_Led2_state;
		DeviceNotifyParameter[3].stringValue = Power_Led3_state;
		DeviceNotifyParameter[4].stringValue = Dimming_state;

		DeviceNotifyParameter[0].time = System_Timer;
		DeviceNotifyParameter[1].time = System_Timer;
		DeviceNotifyParameter[2].time = System_Timer;
		DeviceNotifyParameter[3].time = System_Timer;
		DeviceNotifyParameter[4].time = System_Timer;
		damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,5);

		ucNotify_async_flag = 0;
	#endif
}

void control(){
	Button_Control = digitalRead (BUTTON_PIN1);
	if(Button_Control == LOW){
		if(Button_Control_Buffer == 0){
			Button_Control_Buffer = 1;
			#ifdef DEVICE_SWITCH
				NotifyParameter DeviceNotifyParameter[5];

				
				if(strcmp(Power_state,"OFF") == 0){
					Power_state = "ON";
				}
				else{
					Power_state = "OFF";
				}

				char* sid = "2";
		        DeviceNotifyParameter[0].resourceUri = "/3306/0/5853";
		        DeviceNotifyParameter[1].resourceUri = "/3312/0/5850";
		        DeviceNotifyParameter[2].resourceUri = "/3312/0/5851";
		        DeviceNotifyParameter[3].resourceUri = "/3312/0/5852";
		        DeviceNotifyParameter[4].resourceUri = "/3343/0/5548";

				
				DeviceNotifyParameter[0].stringValue = Power_state;
				DeviceNotifyParameter[1].stringValue = Power_Led1_state;
				DeviceNotifyParameter[2].stringValue = Power_Led2_state;
				DeviceNotifyParameter[3].stringValue = Power_Led3_state;
				DeviceNotifyParameter[4].stringValue = Dimming_state;


				DeviceNotifyParameter[0].time = System_Timer;
				DeviceNotifyParameter[1].time = System_Timer;
				DeviceNotifyParameter[2].time = System_Timer;
				DeviceNotifyParameter[3].time = System_Timer;
				DeviceNotifyParameter[4].time = System_Timer;

		        
		        damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,5);
	        #endif

	        #ifdef DEVICE_PLUG
	        	NotifyParameter DeviceNotifyParameter[4];
		       
		        if(strcmp(Standby_cutoff_state,"true") == 0){
					Standby_cutoff_state = "false";
					Overheat_state = "false";
				}
				else{
					Standby_cutoff_state = "true";
					Overheat_state = "true";
				}

				char* sid = "2";
				DeviceNotifyParameter[0].resourceUri = "/3312/0/5850";
		        DeviceNotifyParameter[1].resourceUri = "/3306/0/5850";
		        DeviceNotifyParameter[2].resourceUri = "/3342/0/5500";
		        DeviceNotifyParameter[3].resourceUri = "/3342/0/5501";

				DeviceNotifyParameter[0].stringValue = Power_state;
				DeviceNotifyParameter[1].stringValue = Standby_cutoff_setting_state;
				DeviceNotifyParameter[2].stringValue = Standby_cutoff_state;
				DeviceNotifyParameter[3].stringValue = Overheat_state;


				DeviceNotifyParameter[0].time = System_Timer;
				DeviceNotifyParameter[1].time = System_Timer;
				DeviceNotifyParameter[2].time = System_Timer;
				DeviceNotifyParameter[3].time = System_Timer;

			    damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,4);
	        #endif
		}
	}
	else{
		Button_Control_Buffer = 0;
	}
}


void Test_Sample(){
	
	NotifyParameter refrigeratorNotifyParam[1];
	NotifyParameter firmwareNotifyParam[2];
	refrigeratorNotifyParam[0].resourceUri = "/3342/0/5500";
	firmwareNotifyParam[0].resourceUri = "/5/0/3";
	firmwareNotifyParam[1].resourceUri = "/5/0/5";
	int i=0;
	char* ti;
	struct timespec tp={0,0};

	if(Serial.available())
	{
		//const char* ch = Serial.read();
		//int command = atoi(ch);
		int ch = Serial.parseInt(); 		//시리얼 입력값을 int값으로 읽어 옮

		switch(ch)
		{

		case 1 : {
			Serial.println("Deregister Device");
			
          	damda_Req_DeregisterDevice();
          	delay(100);
			
			Serial.println("Soft AP ON");
			WiFi.disconnect(true);
			Serial.println("Wifi Disconnect");
			
			delay(100);
			EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			//EEPROM.write(EEPROM_SIZE, 2);		// Soft STA ON
			//EEPROM.write(EEPROM_SIZE, 3);		// Soft AP_STA ON
			EEPROM.commit();
			
			delay(100);
			damda_Read_EEPROM();
  			damda_WiFi_Connect();
			Serial.println("Soft AP MODE START");
			delay(100);
			
			massage_print_flag = 1;
			break;
		}
		case 2 : {
			Serial.println("Soft AP OFF");
			
			WiFi.disconnect(true);
			Serial.println("Wifi Disconnect");
			
          	delay(100);
			
			EEPROM.write(EEPROM_SIZE, 2);		// Soft STA ON
			//EEPROM.write(EEPROM_SIZE, 3);		// Soft AP_STA ON
			EEPROM.commit();
			delay(100);
			
			damda_Read_EEPROM();
  			damda_WiFi_Connect();
  			Serial.println("Soft STATION MODE START");
  			delay(100);
			
			massage_print_flag = 1;
			break;
		}
		case 3 :{
			Serial.println("Mqtt connecting");
			
			delay(100);
	        
			//EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			//EEPROM.write(EEPROM_SIZE, 2);		// Soft STA ON
			EEPROM.write(EEPROM_SIZE, 3);		// Soft AP_STA ON
			EEPROM.commit();
			delay(100);
			
			damda_MQTT_Connect((char*)Topic_Device_id.c_str(),(char*)damda_sample_connection_info.authKey,(char*)damda_sample_connection_info.serverAddr,atoi((char*)damda_sample_connection_info.serverPort),DEVICE_TYPE);
			Serial.println("Mqtt connected");
	        massage_print_flag = 1;
			break;
		}

		case 4 : {
			Serial.println("Register Device");
			
			damda_Req_RegisterDevice();
	   
			break;
		}

		case 5 :{
			Serial.println("Update Device Information");
			
			damda_Req_UpdateDeviceInfo();
        	
			break;
		}
		case 6 :{                 //시간 동기 (동기)
			Serial.println("Timesync ON");
          	
    		damda_Req_SeverTimeInfo();
    		System_Timer_Set = 1;
        	
			break;
		}

		case 7 :{                 //주기 보고
			Serial.println("Report periodic information(notify)");
    		
			ucNotify_sync_flag = 1;
			ucNotify_async_flag = 0;
			
			break;
		}
		case 8 :{                  //주기 보고 종료
       		Serial.println("Report periodic information(notify) STOP");
       		
       		ucNotify_sync_flag = 0;
       		ucNotify_async_flag = 1;
				
			break;
       	}
 		case 9 :{                  //비주기 보고
       		Serial.println("Report event notification(notify)");
			
			Test_Count = 10000;
			ucNotify_async_flag = 1;
			
       	 	break;
  		}
  		case 10 : {
			Serial.println("Unregister Device");
			
			damda_Req_DeregisterDevice();
          	
			break;
		}
		/*case 11 :{
			Serial.println("Deregister Device");

          	if(client.connected()){
          		damda_Req_DeleteDevice();
          	}
          	else {
				Serial.println("MQTT is not connected.");
			}
			break;
		}*/
		case 11 :{
				Serial.println("Firmware Download Init");
			
				damda_Req_UpdateDeviceInfo();
				
				ti = System_Timer;
				firmwareNotifyParam[0].stringValue = "0";
				firmwareNotifyParam[0].time = ti;
				
				firmwareNotifyParam[1].stringValue = "0";
				firmwareNotifyParam[1].time = ti;
				
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,2);// /5/0/3 /5/0/5 and string Value 0,0 send (download init)
				firmwareNotifyParam[0].stringValue="1";
				firmwareNotifyParam[0].time = ti;
				
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/3 and string Value 1 send (downloading)

        	break;
		}
		case 12 :{
			
				Serial.println("Firmware Download");
		        damda_firmware_download();
		        ti = System_Timer;
		        firmwareNotifyParam[0].stringValue="2";
				firmwareNotifyParam[0].time = ti;
				
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/3 and string Value 2 send (download is done)
			
			break;
		}
		case 13 :{
			
				Serial.println("Firmware info update");
				ti = System_Timer;
				firmwareNotifyParam[0].stringValue="3";
				firmwareNotifyParam[0].time = ti;
				
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);	// /5/0/3 and string Value 3 send (firmware installing)
				firmwareNotifyParam[0].stringValue="0";
				firmwareNotifyParam[0].time = ti;
				
				firmwareNotifyParam[1].stringValue="1";
				firmwareNotifyParam[1].time = ti;
				
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,2); 	// /5/0/3 /5/0/5 and string Value 0,1 send (firmware has been installed)
				firmwareNotifyParam[1].stringValue="0";
				damda_setupDeviceInfo(DEVICE_MANUFACTURER,DEVICE_MODEL,DEVICE_SERIAL,firmwareVersion,DEVICE_TYPE);
				damda_Req_UpdateDeviceInfo();
			
			break;
		}
		case 14 :{
			
				Serial.println("Firmware update");

		        damda_firmware_update();
		    
			break;
		}
		/*case 15 :{
			
				Serial.println("Mqtt isconnect");

		        //damda_isMQTTConnected();
		    	Serial.println(damda_IsMQTTConnected());
			break;
		}
		case 16 :{
			
				Serial.println("Mqtt disconnect");

		        damda_DisConnectMQTT();
		    
		break;
		}
		*/
      	default :
      	break;
		}
	}
}
void Device_Port_init(){
	pinMode(BUTTON_PIN, INPUT_PULLUP);    	//버튼
	pinMode(BUTTON_PIN1, INPUT_PULLUP);    	//버튼
	pinMode(LED_BUILTIN9, OUTPUT);    		//상태 LED  기본장착
	pinMode(BUTTON_LED1, OUTPUT);    		//POWER LED
	pinMode(BUTTON_LED2, OUTPUT);    		//LED1
	pinMode(BUTTON_LED3, OUTPUT);    		//LED2
	pinMode(BUTTON_LED4, OUTPUT);    		//LED3
	pinMode(DIMMING_LED, OUTPUT);    		//DEMING

	digitalWrite(LED_BUILTIN9, LOW);
	digitalWrite(BUTTON_LED1, LOW);
	digitalWrite(BUTTON_LED2, LOW);
	digitalWrite(BUTTON_LED3, LOW);
	digitalWrite(BUTTON_LED4, LOW);
	digitalWrite(DIMMING_LED, LOW);

}
void setup() {

  	 Serial.begin(115200);
  	 WiFi.begin(ssid_wifi, pass_wifi);
	   //T_Serial.begin(9600);           	//  가전사와 통신 가능한 RS232 포트 OPEN
     EEPROM.begin(255);                	//  EEPROM Start
     Device_Port_init();

  	 WiFi.disconnect(true);
  	 damda_setupDeviceInfo(DEVICE_MANUFACTURER, DEVICE_MODEL, DEVICE_SERIAL, "1.0", DEVICE_TYPE);

  	
  	Serial.println(damda_getSDKVersion());

   	damda_Read_EEPROM();
  	damda_WiFi_Connect();

	damda_setMQTTMsgReceiveCallback(message_callback);
	
	Serial.print("device_medel :");
	Serial.println(damda_deviceStatusInfo.serial);
	
	damda_deviceStatusInfo = getDeviceInfo();
	//damda_MQTT_Connect((char*)Topic_Device_id.c_str(),(char*)damda_sample_connection_info.authKey,(char*)damda_sample_connection_info.serverAddr,atoi((char*)damda_sample_connection_info.serverPort),(char*)damda_deviceStatusInfo.model);
	damda_SPIFFS();
	massage_print_flag = 1; 							//Commander list 출력
	delay(1000);
  	
	xTaskCreate(
		additionalTask,           // Task 함수 이름
		"additional Task",        // Task 이름
		10000,                    // Task 스택 크기
		NULL,                     // Task 파라미터
		1,                        // Task 우선순위
		NULL
	);                    // Task handle

}

void loop() {
	
	delay(30);
}

// additionalTask 가 생성될 때 이 함수가 호출됨
void additionalTask( void * parameter )
{
	for(;;){
	
		//long now = millis();
		//delay(10);
		//if (now - lastMsg > test_para) {							//5초에 한번식 실행
		//lastMsg = now;
		server.handleClient();
		damda_messageReceived_callback_loop();
		//client.loop();


		Test_Count1++;
		if(Test_Count1 > 10){
			Test_Count1 = 0;
			damda_Reset_EEPROM();										//리셋버튼이 눌리면 EEPROM 초기화
			Test_Command();												//테스트 컴멘더 리스트 출력
			LED_Contol();												//LED 제어
			Test_Sample(); 												//테스트 샘플 제어 명령어
			control();
		}

	    Test_Count++;
	    if(Test_Count >= 10000){									//5초 1회 보고
	    	Test_Count = 0;

	    	
	    	//Serial.print(WiFi.RSSI());

	      	if (WiFi.status() == WL_CONNECTED) {        			//WiFi가 연결 되면
	      		if(ucNotify_sync_flag == 1 || ucNotify_async_flag == 1)Notify_sync();
	      	}
	      	else{                                               	//WiFi가 연결 되지 않았으면.
	        	Serial.print("SYSTEM RESTART ");
	        	ESP.restart();
	      	}

	    }
	}

	// 종료되면 Task 삭제
	// 하지만 위의 무한루프 때문에 호출될 수 없음
	vTaskDelete( NULL );
}
